import sys, os, subprocess, time
import threading  # add at top if not already imported

from PySide6.QtCore import Qt, QMetaObject
from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QTabWidget,
    QLabel,
    QHBoxLayout,
    QVBoxLayout,
    QWidget,
    QPushButton,
    QFileDialog,
    QCheckBox,
    QSpinBox,
    QSizePolicy,
    QTableWidget,
    QTableWidgetItem,
    QStatusBar,
    QProgressBar,
    QLineEdit,
    QMessageBox,
    QGridLayout,
    QDialog,
    QSplitter
)
from PySide6.QtGui import QFont, QIcon
# import metaobject
import configparser, json
import H2GConfig_windows
import H2GDAQ_core
import H2GDAQ_components

from loguru import logger

log_format = "<level>{time:YYYY-MM-DD HH:mm:ss} | {level:.1s} | {message}</level>"

# Configure the logger with the new format
logger.remove()  # Remove default handler
logger.add(sink=sys.stderr, format=log_format)

software_version = "0.11"

class MainWindow(QMainWindow):
    def __init__(self, num_fpgas, num_asics, config_file):
        super().__init__()

        self.i2c_default_file = "config/default_2024Aug_config.json"
        self.i2c_default_json = json.load(open(self.i2c_default_file, 'r'))
        self.i2c_top_reg = self.i2c_default_json["Register Settings"]["Top                 "]
        self.i2c_top_reg = [int(x, 16) for x in self.i2c_top_reg.split()]
        self.i2c_top_reg[0] = self.i2c_top_reg[0] | 0x03
        self.i2c_top_reg = self.i2c_top_reg[:8]

        self.is_muted = False

        self.version = software_version
        # -- Updated modern UI style definitions below --
        self.main_window_format = "background-color: #ffffff; color: #333333; font-family: 'Segoe UI'; font-size: 12px;"
        self.main_widget_format = "background-color: #ffffff; color: #333333; font-family: 'Segoe UI'; font-size: 12px;"
        self.module_widget_background = "background-color: #f7f7f7;"
        self.module_title_format = "font-weight: bold; font-family: 'Segoe UI'; font-size: 14px; color: #333333;"
        self.module_content_format = "font-family: 'Segoe UI'; font-size: 13px; color: #444444;"
        self.common_button_format = """
            QPushButton {
                font-size: 12px;
                color: #333333;
                background-color: #e0e0e0;
                border: none;
                padding: 4px 12px;
                border-radius: 4px;
            }
            QPushButton:hover {
                background-color: #d5d5d5;
            }
            QPushButton:disabled {
                color: #888888;
                background-color: #f0f0f0;
            }
        """
        self.common_spinbox_format_2 = "font-size: 12px; color: #333333; font-family: 'Segoe UI';"
        # spinbox formatting
        spinbox_button_width = 11
        spinbox_button_height = 7
        spinbox_button_tall_height = 14
        spinbox_button_background = "#e0e0e0"
        spinbox_arrow_width = 9
        spinbox_arrow_height = 7
        spinbox_arrow_tall_height = 12
        self.common_spinbox_format = """
        QSpinBox {
            font-size: 12px;
            color: #333333;
            background-color: #ffffff;
            border: 1px solid #ccc;
            border-radius: 4px;
            padding: 2px 4px;
        }
        QSpinBox::up-button {
            width: 12px;
            height: 8px;
            subcontrol-origin: border;
            subcontrol-position: top right;
            image: url(asset/angle-up.png);
            margin-right: 3px;
            margin-top: 2px;
        }
        QSpinBox::down-button {
            width: 12px;
            height: 8px;
            subcontrol-origin: border;
            subcontrol-position: bottom right;
            image: url(asset/angle-down.png);
            margin-right: 3px;
            margin-bottom: 2px;
        }
        QSpinBox::up-arrow, QSpinBox::down-arrow {
            width: 8px;
            height: 8px;
        }
        """
        self.tall_spinbox_format = """
        QSpinBox {
            font-size: 12px;
            color: #333333;
            background-color: #ffffff;
            border: 1px solid #ccc;
            border-radius: 4px;
            padding: 2px 4px;
        }
        QSpinBox::up-button {
            width: 14px;
            height: 12px;
            subcontrol-origin: border;
            subcontrol-position: top right;
            image: url(asset/angle-up.png);
            margin-right: 3px;
            margin-top: 2px;
        }
        QSpinBox::down-button {
            width: 14px;
            height: 12px;
            subcontrol-origin: border;
            subcontrol-position: bottom right;
            image: url(asset/angle-down.png);
            margin-right: 3px;
            margin-bottom: 2px;
        }
        QSpinBox::up-arrow, QSpinBox::down-arrow {
            width: 9px;
            height: 7px;
        }
        """
        self.textinput_format = "font-size: 12px; color: #333333; background-color: #ffffff; border: 1px solid #ccc; border-radius: 4px; padding: 2px 4px;"
        self.divider_label_format = "font-size: 12px; color: #666666; font-family: 'Segoe UI'; font-weight: bold;"
        self.status_sheet_background = "background-color: #ffffff; color: #333333; font-family: 'Segoe UI'; font-size: 12px;"
        # -- End updated style definitions --

        self.status_sheet_list = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16-17", "18-19", "20", "21-23", "24", "25-26", "27", "28", "29", "30", "31", "32-35", "36", "37-39"]
        self.status_item_label = [
            "Header", "FPGA_Board_Address", "Packet_Type", "b2 - CLK_SEL"
        ]
        while self.status_item_label.__len__() < 40:
            self.status_item_label.append("")

        self.gen_config_spinbox_width = 50

        self.num_fpga_tabs = num_fpgas
        self.num_asic_tabs = num_asics

        self.stop_DAQ_flag = False
        self.auto_send_gen_settings = False

        self.data_save_to_file = True

        # Monitoring variables
        self.run_online_monitoring = False
        self.monitoring_executable = ""
        self.monitoring_process = None

        # set to current working directory
        self.selected_working_folder = os.getcwd()

        self.udp_config_json_name = 'config/udp_config.json'
        self.pc_ip = '127.0.0.1'
        self.pc_port = 11000
        self.link_ip_address_list = ['undefined ip' for _ in range(self.num_fpga_tabs)]
        self.link_port_list = ['undefined port' for _ in range(self.num_fpga_tabs)]

        with open(self.udp_config_json_name, 'r') as _udp_config:
            udp_config_json = json.load(_udp_config)
            for key in list(udp_config_json['KCU_UDP'].keys()):
                if "FPGA" in key:
                    key_index = int(key.split(" ")[1])
                    if key_index < len(self.link_ip_address_list):
                        self.link_ip_address_list[key_index] = udp_config_json['KCU_UDP'][key]['ip']
                        self.link_port_list[key_index] = udp_config_json['KCU_UDP'][key]['port']
            self.pc_ip = udp_config_json['PC_UDP']['ip']
            self.pc_port = int(udp_config_json['PC_UDP']['port'])

        self.worker = H2GDAQ_core.UdpSocketWorker(self, self.pc_ip, self.pc_port)

        self.current_DAQ_mode = "manual"
        self.DAQ_timer_sec = 0
        self.DAQ_event_cnt = 0
        self.current_RunNumber = 0
        self.send_gen_before_DAQ = False
        self.reset_cnt_before_DAQ = False

        self.DAQ_completed_flag = True

        self.hv_checkboxes = []
        self.hv_fpga_checkboxes = []
        self.hv_settings = [False for _ in range(self.num_fpga_tabs * self.num_asic_tabs)]

        self.daq_gen_start_stop_values = {
            "daq_push": 0x00,
            "generator_start_stop": 0x00,
            "daq_start_stop": 0x00
        }

        self.daq_gen_start_stop_label_str = {
            "daq_push": "DAQ Push [7:0]",
            "generator_start_stop": "Generator Start/Stop",
            "daq_start_stop": "DAQ Start/Stop [7:0]"
        }

        self.daq_gen_start_stop_max = {
            "daq_push": 0xFF,
            "generator_start_stop": 0x01,
            "daq_start_stop": 0xFF
        }

        self.daq_gen_start_stop_UI_type = {
            "daq_push": "spinbox",
            "generator_start_stop": "checkbox",
            "daq_start_stop": "spinbox"
        }
        
        self.daq_gen_start_stop_Qt_objects = {}

        self.gen_setting_values = {
            "data_coll_enable": 0x00,
            "trig_coll_enable": 0x00,
            "daq_fcmd": 0x00,
            "generator_pre_fcmd": 0x00,
            "generator_fcmd": 0x00,
            "ext_trig_enable": 0x00,
            "ext_trig_delay": 0x00,
            "ext_trig_deadtime": 0x00,
            "generator_preimp_enable": 0x00,
            "generator_pre_interval": 0x00,
            "generator_nr_of_cycles": 0x00,
            "generator_interval": 0x00,
            "daq_push_fcmd": 0x00,
            "machine_gun": 0x00
        }

        self.gen_settings_label_str = {
            "label_data_coll":          "Data Collection",
            "data_coll_enable":         "Data Coll En[7:0]",
            "trig_coll_enable":         "Trig Coll En[7:0]",
            "label_gen":                "Generator",
            "generator_preimp_enable":  "Gen PreImp En    ",
            "generator_pre_interval":   "Gen Pre Int      ",
            "generator_nr_of_cycles":   "Gen Nr Cycles    ",
            "generator_interval":       "Gen Interval     ",
            "label_ext_trig":           "External Trigger",
            "ext_trig_enable":          "Ext Trg En       ",
            "ext_trig_delay":           "Ext Trg Delay    ",
            "ext_trig_deadtime":        "Ext Trg DeadT    ",
            "label_fcmd":               "Fast Command",
            "daq_fcmd":                 "DAQ FCMD         ",
            "daq_push_fcmd":            "DAQ Push FCMD    ",
            "generator_pre_fcmd":       "Gen Pre FCMD     ",
            "generator_fcmd":           "Gen FCMD         ",
            "label_machine_gun":        "Machine Gun",
            "machine_gun":              "Machine Gun      "
        }

        self.gen_settings_max = {
            "data_coll_enable": 0xFF,
            "trig_coll_enable": 0xFF,
            "daq_fcmd": 0xFF,
            "generator_pre_fcmd": 0xFF,
            "generator_fcmd": 0xFF,
            "ext_trig_enable": 0x01,
            "ext_trig_delay": 0xFF,
            "ext_trig_deadtime": 0xFFFF,
            "generator_preimp_enable": 0x01,
            "generator_pre_interval": 0xFFFF,
            "generator_nr_of_cycles": 0xFFFFFFFF,
            "generator_interval": 0xFFFFFFFF,
            "daq_push_fcmd": 0xFF,
            "machine_gun": 0xFF
        }

        self.gen_settings_UI_type = {
            "data_coll_enable": "spinbox",
            "trig_coll_enable": "spinbox",
            "daq_fcmd": "spinbox",
            "generator_pre_fcmd": "spinbox",
            "generator_fcmd": "spinbox",
            "ext_trig_enable": "checkbox",
            "ext_trig_delay": "spinbox",
            "ext_trig_deadtime": "textinput",
            "generator_preimp_enable": "checkbox",
            "generator_pre_interval": "spinbox",
            "generator_nr_of_cycles": "textinput",
            "generator_interval": "textinput",
            "daq_push_fcmd": "spinbox",
            "machine_gun": "spinbox"
        }

        self.reset_adj_Qt_objects = {}

        self.reset_adj_values = {
            "SW_HARD_RESET_SELECT": 0x00,
            "SW_HARD_RESET": 0x00,
            "SW_SOFT_RESET_SELECT": 0x00,
            "SW_SOFT_RESET": 0x00,
            "SW_I2C_RESET_SELECT": 0x00,
            "SW_I2C_RESET": 0x00,
            "RESET_PACK_COUNTER": 0x00,
            "ADJUSTABLE_START": 0x00
        }

        self.reset_adj_label_str = {
            "SW_HARD_RESET_SELECT": "Hard Reset Sel[7:0]",
            "SW_HARD_RESET": "Hard Reset",
            "SW_SOFT_RESET_SELECT": "Soft Reset Sel[7:0]",
            "SW_SOFT_RESET": "Soft Reset",
            "SW_I2C_RESET_SELECT": "I2C Reset Sel[7:0]",
            "SW_I2C_RESET": "I2C Reset",
            "RESET_PACK_COUNTER": "Reset Pack Cnt[7:0]",
            "ADJUSTABLE_START": "Adjustable Start[7:0]"
        }

        self.reset_adj_max = {
            "SW_HARD_RESET_SELECT": 0xFF,
            "SW_HARD_RESET": 0x01,
            "SW_SOFT_RESET_SELECT": 0xFF,
            "SW_SOFT_RESET": 0x01,
            "SW_I2C_RESET_SELECT": 0xFF,
            "SW_I2C_RESET": 0x01,
            "RESET_PACK_COUNTER": 0xFF,
            "ADJUSTABLE_START": 0xFF
        }

        self.reset_adj_UI_type = {
            "SW_HARD_RESET_SELECT": "spinbox",
            "SW_HARD_RESET": "checkbox",
            "SW_SOFT_RESET_SELECT": "spinbox",
            "SW_SOFT_RESET": "checkbox",
            "SW_I2C_RESET_SELECT": "spinbox",
            "SW_I2C_RESET": "checkbox",
            "RESET_PACK_COUNTER": "spinbox",
            "ADJUSTABLE_START": "spinbox"
        }

        self.gen_settings_Qt_objects = {}

        self.auto_phase_scan_active = False  # initialize flag

        self.initUI()

        self.config_file = config_file
        self.loadIniConfig()

        H2GDAQ_components.on_stop_DAQ_core(self)

        self.setStyleSheet(self.main_window_format)
        self.setCentralWidget(self.main_tabs)
        self.resize(800, 600)

    def initUI(self):
        self.setWindowTitle("H2GDAQ")
        self.main_tabs = QTabWidget()
        self.main_tabs.setStyleSheet(self.main_widget_format)
        self.main_tabs.setTabPosition(QTabWidget.West)
        self.main_tabs.setMovable(False)
        self.main_tabs.setDocumentMode(True) # for MacOS to have similar look

        # Add tabs
        tabDAQ_main_widget = QWidget()
        tabDAQ_main_widget.setStyleSheet(self.module_widget_background)
        
        tabDAQ_main_layout = QHBoxLayout()
        tabDAQ_splitter = QSplitter(Qt.Horizontal)  # Use QSplitter for dynamic resizing
        tabDAQ_main_layout.addWidget(tabDAQ_splitter)

        tabDAQ_L1_widget = QWidget()
        tabDAQ_L1_layout = QVBoxLayout()
        tabDAQ_L1_widget.setLayout(tabDAQ_L1_layout)
        tabDAQ_L1_widget.setStyleSheet(self.module_widget_background)

        tabDAQ_L2_widget = QWidget()
        tabDAQ_L2_layout = QVBoxLayout()
        tabDAQ_L2_widget.setLayout(tabDAQ_L2_layout)
        tabDAQ_L2_widget.setStyleSheet(self.module_widget_background)

        tabDAQ_L3_widget = QWidget()
        tabDAQ_L3_layout = QVBoxLayout()
        tabDAQ_L3_widget.setLayout(tabDAQ_L3_layout)
        tabDAQ_L3_widget.setStyleSheet(self.module_widget_background)

        tabDAQ_splitter.addWidget(tabDAQ_L1_widget)
        tabDAQ_splitter.addWidget(tabDAQ_L2_widget)
        tabDAQ_splitter.addWidget(tabDAQ_L3_widget)

        tabDAQ_splitter.setStretchFactor(0, 1)  # L1
        tabDAQ_splitter.setStretchFactor(1, 2)  # L2
        tabDAQ_splitter.setStretchFactor(2, 1)  # L3

        tabDAQ_main_widget.setLayout(tabDAQ_main_layout)
        self.main_tabs.addTab(tabDAQ_main_widget, "DAQ")

        # * --- DAQ L1 --- *
        tabDAQ_L1_workingfolder_widget = QWidget(tabDAQ_L1_layout.widget())
        tabDAQ_L1_workingfolder_layout = QVBoxLayout()
        tabDAQ_L1_workingfolder_widget.setLayout(tabDAQ_L1_workingfolder_layout)
        tabDAQ_L1_workingfolder_widget.setStyleSheet(self.module_widget_background)
        tabDAQ_L1_layout.addWidget(tabDAQ_L1_workingfolder_widget, stretch=1)

        tabDAQ_L1_online_monitoring_widget = QWidget(tabDAQ_L1_layout.widget())
        tabDAQ_L1_online_monitoring_layout = QVBoxLayout()
        tabDAQ_L1_online_monitoring_widget.setLayout(tabDAQ_L1_online_monitoring_layout)
        tabDAQ_L1_online_monitoring_widget.setStyleSheet(self.module_widget_background)
        tabDAQ_L1_layout.addWidget(tabDAQ_L1_online_monitoring_widget, stretch=1)

        tabDAQ_L1_daq_settings_widget = QWidget(tabDAQ_L1_layout.widget())
        tabDAQ_L1_daq_settings_layout = QVBoxLayout()
        tabDAQ_L1_daq_settings_layout.setSpacing(15)
        tabDAQ_L1_daq_settings_widget.setLayout(tabDAQ_L1_daq_settings_layout)
        tabDAQ_L1_daq_settings_widget.setStyleSheet(self.module_widget_background)
        tabDAQ_L1_layout.addWidget(tabDAQ_L1_daq_settings_widget, stretch=3)

        # * ---- Add working folder components ---- *
        tabDAQ_L1_workingfolder_title = QLabel("Working Folder", alignment=Qt.AlignLeft)
        tabDAQ_L1_workingfolder_title.setStyleSheet(self.module_title_format)
        tabDAQ_L1_workingfolder_layout.addWidget(tabDAQ_L1_workingfolder_title)

        self.tabDAQ_L1_workingfolder_workingfolder = QLabel(self.selected_working_folder, alignment=Qt.AlignLeft)
        self.tabDAQ_L1_workingfolder_workingfolder.setStyleSheet(self.module_content_format)
        # auto wrap text
        self.tabDAQ_L1_workingfolder_workingfolder.setWordWrap(True)
        tabDAQ_L1_workingfolder_layout.addWidget(self.tabDAQ_L1_workingfolder_workingfolder)

        tabDAQ_L1_workingfolder_buttons_layout = QHBoxLayout()
        tabDAQ_L1_workingfolder_layout.addLayout(tabDAQ_L1_workingfolder_buttons_layout)

        tabDAQ_L1_workingfolder_select_button = QPushButton("Select Folder")
        tabDAQ_L1_workingfolder_open_button   = QPushButton("Open Folder")
        self.tabDAQ_L1_workingfolder_save_options_button = QPushButton("Print to Terminal")
        
        tabDAQ_L1_workingfolder_select_button.setFixedHeight(20)
        tabDAQ_L1_workingfolder_open_button.setFixedHeight(20)
        self.tabDAQ_L1_workingfolder_save_options_button.setFixedHeight(20)
        tabDAQ_L1_workingfolder_select_button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        tabDAQ_L1_workingfolder_open_button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        self.tabDAQ_L1_workingfolder_save_options_button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)

        tabDAQ_L1_workingfolder_select_button.setStyleSheet(self.common_button_format)
        tabDAQ_L1_workingfolder_open_button.setStyleSheet(self.common_button_format)
        self.tabDAQ_L1_workingfolder_save_options_button.setStyleSheet(self.common_button_format)

        tabDAQ_L1_workingfolder_select_button.clicked.connect(self.on_workingfolder_select)
        tabDAQ_L1_workingfolder_open_button.clicked.connect(self.on_workingfolder_open)
        self.tabDAQ_L1_workingfolder_save_options_button.clicked.connect(self.on_save_options)

        tabDAQ_L1_workingfolder_buttons_layout.addWidget(tabDAQ_L1_workingfolder_select_button)
        # tabDAQ_L1_workingfolder_buttons_layout.addWidget(tabDAQ_L1_workingfolder_open_button)
        tabDAQ_L1_workingfolder_buttons_layout.addWidget(self.tabDAQ_L1_workingfolder_save_options_button)

        # Add online monitoring settings

        # * ---- Add DAQ settings components ---- *
        tabDAQ_L1_daq_settings_title = QLabel("DAQ Settings", alignment=Qt.AlignLeft)
        tabDAQ_L1_daq_settings_title.setStyleSheet(self.module_title_format)
        tabDAQ_L1_daq_settings_vbox = QHBoxLayout()

        tabDAQ_L1_daq_settings_vbox.addWidget(tabDAQ_L1_daq_settings_title)

        self.mute_icon_unmute = "asset/unmute-96.png"
        self.mute_icon_mute = "asset/mute-96.png"
        self.mute_button = QPushButton("Mute")
        self.mute_button.setIcon(QIcon(self.mute_icon_unmute))
        self.mute_button.setStyleSheet(self.common_button_format)
        self.mute_button.setFixedHeight(20)
        self.mute_button.setFixedWidth(30)
        self.mute_button.setText("")
        self.mute_button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        self.mute_button.clicked.connect(lambda: H2GDAQ_components.on_mute_button_pushed(self))
        tabDAQ_L1_daq_settings_vbox.addWidget(self.mute_button)

        tabDAQ_L1_daq_settings_layout.addLayout(tabDAQ_L1_daq_settings_vbox)

        tabDAQ_L1_daq_settings_event_cnt_layout = QHBoxLayout()
        tabDAQ_L1_daq_settings_layout.addLayout(tabDAQ_L1_daq_settings_event_cnt_layout)
        self.event_cnt_select = QCheckBox(tabDAQ_L1_daq_settings_event_cnt_layout.widget())
        self.event_cnt_select.setChecked(True)
        self.event_cnt_select.clicked.connect(self.on_event_mode_selected)
        tabDAQ_L1_daq_settings_event_cnt_layout.addWidget(self.event_cnt_select, alignment=Qt.AlignLeft)

        event_cnt_label = QLabel("event", alignment=Qt.AlignCenter)
        event_cnt_label.setStyleSheet(self.module_content_format)
        tabDAQ_L1_daq_settings_event_cnt_layout.addWidget(event_cnt_label, alignment=Qt.AlignCenter)

        self.event_cnt_box = QSpinBox(tabDAQ_L1_daq_settings_event_cnt_layout.widget())
        self.event_cnt_box.setMinimum(1)
        self.event_cnt_box.setMaximum(1000000)
        self.event_cnt_box.setValue(1000)
        self.event_cnt_box.setStyleSheet(self.common_spinbox_format)
        self.event_cnt_box.valueChanged.connect(self.on_event_cnt_changed)
        tabDAQ_L1_daq_settings_event_cnt_layout.addWidget(self.event_cnt_box, alignment=Qt.AlignRight)

        tabDAQ_L1_daq_settings_time_cnt_layout = QHBoxLayout()
        tabDAQ_L1_daq_settings_layout.addLayout(tabDAQ_L1_daq_settings_time_cnt_layout)
        self.time_cnt_select = QCheckBox(tabDAQ_L1_daq_settings_time_cnt_layout.widget())
        self.time_cnt_select.setChecked(True)
        self.time_cnt_select.clicked.connect(self.on_timed_mode_selected)
        tabDAQ_L1_daq_settings_time_cnt_layout.addWidget(self.time_cnt_select, alignment=Qt.AlignLeft)

        time_cnt_label = QLabel("time [sec]", alignment=Qt.AlignCenter)
        time_cnt_label.setStyleSheet(self.module_content_format)
        tabDAQ_L1_daq_settings_time_cnt_layout.addWidget(time_cnt_label, alignment=Qt.AlignCenter)

        self.time_cnt_box = QSpinBox(tabDAQ_L1_daq_settings_time_cnt_layout.widget())
        self.time_cnt_box.setMinimum(1)
        self.time_cnt_box.setMaximum(1000000)
        self.time_cnt_box.setValue(1000)
        self.time_cnt_box.setStyleSheet(self.common_spinbox_format)
        self.time_cnt_box.valueChanged.connect(self.on_timed_sec_changed)
        tabDAQ_L1_daq_settings_time_cnt_layout.addWidget(self.time_cnt_box, alignment=Qt.AlignRight)

        tabDAQ_L1_daq_settings_manual_layout = QHBoxLayout()
        tabDAQ_L1_daq_settings_layout.addLayout(tabDAQ_L1_daq_settings_manual_layout)
        self.manual_select = QCheckBox(tabDAQ_L1_daq_settings_manual_layout.widget())
        self.manual_select.setChecked(True)
        self.manual_select.clicked.connect(self.on_manual_mode_selected)
        tabDAQ_L1_daq_settings_manual_layout.addWidget(self.manual_select, alignment=Qt.AlignLeft)

        manual_label = QLabel("manual", alignment=Qt.AlignCenter)
        manual_label.setStyleSheet(self.module_content_format)
        tabDAQ_L1_daq_settings_manual_layout.addWidget(manual_label, alignment=Qt.AlignRight)

        tabDAQ_L1_daq_settings_layout.addStretch()

        tabDAQ_L1_daq_settings_reset_cnt_layout = QHBoxLayout()
        tabDAQ_L1_daq_settings_layout.addLayout(tabDAQ_L1_daq_settings_reset_cnt_layout)
        self.reset_cnt_checkbox = QCheckBox(tabDAQ_L1_daq_settings_reset_cnt_layout.widget())
        self.reset_cnt_checkbox.setChecked(False)
        self.reset_cnt_checkbox.clicked.connect(lambda: H2GDAQ_components.on_reset_cnt_auto_select_checkbox_changed(self, status=self.reset_cnt_checkbox.isChecked()))
        self.reset_cnt_label = QLabel("Reset Pack Counter")
        self.reset_cnt_label.setStyleSheet(self.module_content_format)
        tabDAQ_L1_daq_settings_reset_cnt_layout.addWidget(self.reset_cnt_checkbox, alignment=Qt.AlignLeft)
        tabDAQ_L1_daq_settings_reset_cnt_layout.addWidget(self.reset_cnt_label, alignment=Qt.AlignRight)

        tabDAQ_L1_monitoring_settings_title = QLabel("Online Monitoring", alignment=Qt.AlignLeft)
        tabDAQ_L1_monitoring_settings_title.setStyleSheet(self.module_title_format)
        tabDAQ_L1_online_monitoring_layout.addWidget(tabDAQ_L1_monitoring_settings_title)

        tabDAQ_L1_daq_settings_run_monitoring_layout = QHBoxLayout()
        tabDAQ_L1_online_monitoring_layout.addLayout(tabDAQ_L1_daq_settings_run_monitoring_layout)
        self.run_monitoring_checkbox = QCheckBox(tabDAQ_L1_daq_settings_run_monitoring_layout.widget())
        self.run_monitoring_checkbox.setChecked(False)
        self.run_monitoring_checkbox.clicked.connect(lambda: H2GDAQ_components.on_run_monitoring_checkbox_changed(self, status=self.run_monitoring_checkbox.isChecked()))
        self.run_monitoring_label = QLabel("Run Monitoring")
        self.run_monitoring_label.setStyleSheet(self.module_content_format)
        tabDAQ_L1_daq_settings_run_monitoring_layout.addWidget(self.run_monitoring_checkbox, alignment=Qt.AlignLeft)
        tabDAQ_L1_daq_settings_run_monitoring_layout.addWidget(self.run_monitoring_label, alignment=Qt.AlignRight)

        tabDAQ_L1_monitoring_executable_select = QPushButton("Select Monitoring Folder")
        tabDAQ_L1_monitoring_executable_select.setStyleSheet(self.common_button_format)
        tabDAQ_L1_monitoring_executable_select.clicked.connect(self.on_set_monitoring_executable)
        tabDAQ_L1_online_monitoring_layout.addWidget(tabDAQ_L1_monitoring_executable_select)

        self.tabDAQ_L1_monitoring_executable_path = QLabel(self.monitoring_executable, alignment=Qt.AlignLeft)
        self.tabDAQ_L1_monitoring_executable_path.setStyleSheet(self.module_content_format)
        # auto wrap text
        self.tabDAQ_L1_monitoring_executable_path.setWordWrap(True)
        tabDAQ_L1_online_monitoring_layout.addWidget(self.tabDAQ_L1_monitoring_executable_path)

        tabDAQ_L1_daq_settings_runnum_layout = QHBoxLayout()
        tabDAQ_L1_daq_settings_layout.addLayout(tabDAQ_L1_daq_settings_runnum_layout)
        self.runnum_label = QLabel("Run Number ")
        self.runnum_label.setStyleSheet(self.module_content_format)
        tabDAQ_L1_daq_settings_runnum_layout.addWidget(self.runnum_label, alignment=Qt.AlignLeft)

        data_cnt_layout = QHBoxLayout()
        self.data_cnt_title_label = QLabel("Data Count")
        self.data_cnt_title_label.setStyleSheet(self.module_content_format)
        data_cnt_layout.addWidget(self.data_cnt_title_label, alignment=Qt.AlignLeft)

        self.data_cnt_label = QLabel("-- byte")
        self.data_cnt_label.setStyleSheet(self.module_content_format)
        data_cnt_layout.addWidget(self.data_cnt_label, alignment=Qt.AlignRight)
        tabDAQ_L1_daq_settings_layout.addLayout(data_cnt_layout)
        
        self.runnum_box = QSpinBox(tabDAQ_L1_daq_settings_runnum_layout.widget())
        self.runnum_box.setMinimum(1)
        self.runnum_box.setMaximum(999)
        self.runnum_box.setFixedHeight(40)
        self.runnum_box.setFixedWidth(80)
        self.runnum_box.setValue(1)
        self.runnum_box.setStyleSheet(self.tall_spinbox_format)
        self.runnum_box.valueChanged.connect(self.on_runnumber_changed)
        tabDAQ_L1_daq_settings_runnum_layout.addWidget(self.runnum_box, alignment=Qt.AlignRight)

        # self.PhaseScan_button = QPushButton("Phase Scan")
        # self.PhaseScan_button.setStyleSheet(self.common_button_format)
        # self.PhaseScan_button.setFixedHeight(25)
        # # self.PhaseScan_button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        # self.PhaseScan_button.clicked.connect(lambda: self.on_phasescan_button_pushed())
        # tabDAQ_L1_daq_settings_layout.addWidget(self.PhaseScan_button)

        # Add new Auto Phase Scan button
        self.autoPhaseScan_button = QPushButton("Auto Phase Scan")
        self.autoPhaseScan_button.setStyleSheet(self.common_button_format)
        self.autoPhaseScan_button.setFixedHeight(25)
        self.autoPhaseScan_button.clicked.connect(self.on_auto_phase_scan_button_pushed)
        tabDAQ_L1_daq_settings_layout.addWidget(self.autoPhaseScan_button)

        self.IODELAY_button = QPushButton("Set IODELAY")
        self.IODELAY_button.setStyleSheet(self.common_button_format)
        self.IODELAY_button.setFixedHeight(25)
        # self.IODELAY_button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.IODELAY_button.clicked.connect(lambda: self.on_iodelay_button_pushed())
        tabDAQ_L1_daq_settings_layout.addWidget(self.IODELAY_button)

        self.DAQ_Start_button = QPushButton("Start DAQ")
        self.DAQ_Start_button.setStyleSheet("QPushButton {font-size: 12px; font-family: 'DejaVu Sans Mono'; color: #404040; background-color: #a3fcf2;} QPushButton:disabled {color: #808080; background-color: #f0f0f0;}")
        self.DAQ_Start_button.setFixedHeight(40)
        # self.DAQ_Start_button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        tabDAQ_L1_daq_settings_layout.addWidget(self.DAQ_Start_button)
        self.DAQ_Start_button.clicked.connect(lambda: H2GDAQ_components.on_start_DAQ(self))

        self.DAQ_Stop_button = QPushButton("Stop DAQ")
        self.DAQ_Stop_button.setStyleSheet("QPushButton {font-size: 12px; font-family: 'DejaVu Sans Mono'; color: #404040; background-color: #f5abbd;} QPushButton:disabled {color: #808080; background-color: #f0f0f0;}")
        self.DAQ_Stop_button.setFixedHeight(40)
        # self.DAQ_Stop_button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        tabDAQ_L1_daq_settings_layout.addWidget(self.DAQ_Stop_button)
        self.DAQ_Stop_button.clicked.connect(lambda: H2GDAQ_components.on_stop_DAQ(self))

        # * --- DAQ L2 --- *
        self.tabDAQ_L2_tabs = QTabWidget()
        self.tabDAQ_L2_tabs.setTabPosition(QTabWidget.North)
        tabDAQ_L2_gen_config_widget = QWidget(self.tabDAQ_L2_tabs)
        tabDAQ_L2_gen_config_layout = QVBoxLayout()
        tabDAQ_L2_gen_config_widget.setLayout(tabDAQ_L2_gen_config_layout)
        tabDAQ_L2_gen_config_widget.setStyleSheet(self.module_widget_background)
        tabDAQ_L2_layout.addWidget(self.tabDAQ_L2_tabs)
        self.tabDAQ_L2_tabs.addTab(tabDAQ_L2_gen_config_widget, "Generator")

        tabDAQ_L2_debug_widget = QWidget(self.tabDAQ_L2_tabs)
        tabDAQ_L2_debug_layout = QVBoxLayout()
        tabDAQ_L2_debug_widget.setLayout(tabDAQ_L2_debug_layout)
        tabDAQ_L2_debug_widget.setStyleSheet(self.module_widget_background)
        self.tabDAQ_L2_tabs.addTab(tabDAQ_L2_debug_widget, "Debug")

        self.debug_table_item = [
            'Header', 'FPGA_Board', 'Packet_Type', 'BX COUNTER', 'S_IO_DLYO_DAQ1', 'S_IO_DLYO_DAQ0', 'Adj. Ready', 'Adj. DQ1', 'Adj. DQ0', 'Adj. TR3', 'Adj. TR2', 'Adj. TR1', 'Adj. TR0', 'Adj. ERROR', 'TRIGGER0', 'TRIGGER1', 'TRIGGER2', 'TRIGGER3', 'DATA0', 'DATA1'
        ]

        # add an table for debug
        self.debug_table = QTableWidget(20, 2)
        self.debug_table.setStyleSheet(self.status_sheet_background)
        # set column width
        self.debug_table.setColumnWidth(0, 120)
        self.debug_table.setColumnWidth(1, 150)
        # set row height
        self.debug_table.verticalHeader().setDefaultSectionSize(25)
        self.debug_table.setHorizontalHeaderLabels(["Item", "Value"])
        # set header text color
        self.debug_table.horizontalHeaderItem(0).setForeground(Qt.black)
        self.debug_table.horizontalHeaderItem(1).setForeground(Qt.black)
        # set grid line color
        self.debug_table.setStyleSheet("QTableWidget {gridline-color: #202020;}")
        # set header grid line color
        self.debug_table.horizontalHeader().setStyleSheet("QHeaderView::section {background-color: #f0f0f0;}")
        # hide the vertical header
        self.debug_table.verticalHeader().setVisible(False)
        # expand the table to fit the window
        self.debug_table.horizontalHeader().setStretchLastSection(True)
        for _row in range(20):
            _tab_widget_label = QTableWidgetItem(self.debug_table_item[_row])
            _tab_widget_label.setFlags(Qt.ItemIsEnabled)
            _tab_widget_label.setTextAlignment(Qt.AlignCenter)
            # set black text color
            _tab_widget_label.setForeground(Qt.black)
            self.debug_table.setItem(_row, 0, _tab_widget_label)

        tabDAQ_L2_debug_layout.addWidget(self.debug_table)

        # tabDAQ_L2_debug_layout.addStretch(1)

        self.tabDAQ_L2_debug_request_button = QPushButton("Request Debug Data")
        self.tabDAQ_L2_debug_request_button.setStyleSheet(self.common_button_format)
        self.tabDAQ_L2_debug_request_button.clicked.connect(lambda: self.on_request_debug_data_button_pushed())
        tabDAQ_L2_debug_layout.addWidget(self.tabDAQ_L2_debug_request_button)

        # tabDAQ_L2_gen_config_title = QLabel("Generator Config", alignment=Qt.AlignLeft)
        # tabDAQ_L2_gen_config_title.setStyleSheet(self.module_title_format)
        # tabDAQ_L2_gen_config_layout.addWidget(tabDAQ_L2_gen_config_title)

        for _gen_setting in self.gen_settings_label_str.keys():
            if "label" in _gen_setting:
                _label_content = self.gen_settings_label_str[_gen_setting]
                _label_content = '--- ' + _label_content + ' ---'
                _gen_setting_label = QLabel(_label_content, alignment=Qt.AlignCenter)
                _gen_setting_label.setStyleSheet(self.divider_label_format)
                tabDAQ_L2_gen_config_layout.addWidget(_gen_setting_label)
                continue
            _gen_setting_layout = QHBoxLayout()
            tabDAQ_L2_gen_config_layout.addLayout(_gen_setting_layout)

            _gen_setting_label = QLabel(self.gen_settings_label_str[_gen_setting], alignment=Qt.AlignLeft)
            _gen_setting_label.setStyleSheet(self.module_content_format)
            _gen_setting_layout.addWidget(_gen_setting_label)

            if self.gen_settings_UI_type[_gen_setting] == "spinbox":
                _gen_setting_spinbox = QSpinBox()
                _gen_setting_spinbox.setMinimum(0)
                _gen_setting_spinbox.setMaximum(self.gen_settings_max[_gen_setting])
                if self.gen_setting_values[_gen_setting] < self.gen_settings_max[_gen_setting]:
                    _gen_setting_spinbox.setValue(self.gen_setting_values[_gen_setting])
                
                _gen_setting_spinbox.setStyleSheet(self.common_spinbox_format)
                _gen_setting_spinbox.setFixedWidth(self.gen_config_spinbox_width)
                self.gen_settings_Qt_objects[_gen_setting] = _gen_setting_spinbox
                _gen_setting_spinbox.valueChanged.connect(lambda value, _gen_setting=_gen_setting: H2GDAQ_components.on_gen_setting_changed_spinbox(self, value, _gen_setting))
                _gen_setting_layout.addWidget(_gen_setting_spinbox, alignment=Qt.AlignRight)
            elif self.gen_settings_UI_type[_gen_setting] == "checkbox":
                _gen_setting_checkbox = QCheckBox()
                _gen_setting_checkbox.setChecked(self.gen_setting_values[_gen_setting])
                self.gen_settings_Qt_objects[_gen_setting] = _gen_setting_checkbox
                _gen_setting_checkbox.stateChanged.connect(lambda state, _gen_setting=_gen_setting: H2GDAQ_components.on_gen_setting_changed_checkbox(self, state == 2, _gen_setting))
                _gen_setting_layout.addWidget(_gen_setting_checkbox, alignment=Qt.AlignRight)
            elif self.gen_settings_UI_type[_gen_setting] == "textinput":
                _gen_setting_textinput = QLineEdit()
                _gen_setting_textinput.setText(str(self.gen_setting_values[_gen_setting]))
                _gen_setting_textinput.setStyleSheet(self.textinput_format)
                _gen_setting_textinput.setFixedWidth(self.gen_config_spinbox_width)
                self.gen_settings_Qt_objects[_gen_setting] = _gen_setting_textinput
                _gen_setting_textinput.textChanged.connect(lambda text, _gen_setting=_gen_setting: H2GDAQ_components.on_gen_setting_changed_lineedit(self, text, _gen_setting))
                _gen_setting_layout.addWidget(_gen_setting_textinput, alignment=Qt.AlignRight)

        tabDAQ_L2_gen_config_layout.addStretch(1)

        self.gen_settings_auto_select = QCheckBox()
        self.gen_settings_auto_select.setChecked(True)
        self.gen_settings_auto_select.clicked.connect(lambda: H2GDAQ_components.on_gen_settings_auto_select_checkbox_changed(self, status=self.gen_settings_auto_select.isChecked()))

        gen_settings_auto_label = QLabel("Send Gen Config before DAQ")
        gen_settings_auto_label.setStyleSheet(self.module_content_format)

        tabDAQ_L2_gen_config_auto_layout = QHBoxLayout()
        tabDAQ_L2_gen_config_layout.addLayout(tabDAQ_L2_gen_config_auto_layout)
        tabDAQ_L2_gen_config_auto_layout.addWidget(gen_settings_auto_label, alignment=Qt.AlignLeft)
        tabDAQ_L2_gen_config_auto_layout.addWidget(self.gen_settings_auto_select, alignment=Qt.AlignRight)

        self.gen_settings_apply_button = QPushButton("Send Gen Config")
        self.gen_settings_apply_button.setStyleSheet(self.common_button_format + "background-color: #a3fcf2;")
        # self.gen_settings_apply_button.setFixedHeight(20)
        self.gen_settings_apply_button.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        self.gen_settings_apply_button.clicked.connect(lambda: H2GDAQ_components.on_gen_settings_apply_button_pushed(self))
        tabDAQ_L2_gen_config_layout.addWidget(self.gen_settings_apply_button)


        # * --- DAQ L3 --- *
        tabDAQ_L3_info_widget = QWidget(tabDAQ_L3_layout.widget())
        tabDAQ_L3_info_layout = QVBoxLayout()
        tabDAQ_L3_info_widget.setLayout(tabDAQ_L3_info_layout)
        tabDAQ_L3_info_widget.setStyleSheet(self.module_widget_background)
        tabDAQ_L3_layout.addWidget(tabDAQ_L3_info_widget)

        # add tool layout
        
        # tabDAQ_L3_info_layout.addWidget(tabDAQ_L3_tool_layout)

        tabDAQ_L3_gen_config_title = QLabel("DAQ + Generator START/STOP", alignment=Qt.AlignLeft)
        tabDAQ_L3_gen_config_title.setStyleSheet(self.module_title_format)
        tabDAQ_L3_info_layout.addWidget(tabDAQ_L3_gen_config_title)

        # for _key in self.daq_gen_start_stop_label_str.keys():
        #     _gen_start_stop_layout = QHBoxLayout()
        #     tabDAQ_L3_info_layout.addLayout(_gen_start_stop_layout)

        #     _gen_start_stop_label = QLabel(self.daq_gen_start_stop_label_str[_key], alignment=Qt.AlignLeft)
        #     _gen_start_stop_label.setStyleSheet(self.module_content_format)
        #     _gen_start_stop_layout.addWidget(_gen_start_stop_label)

        #     if self.daq_gen_start_stop_UI_type[_key] == "spinbox":
        #         _gen_start_stop_spinbox = QSpinBox()
        #         _gen_start_stop_spinbox.setMinimum(0)
        #         _gen_start_stop_spinbox.setMaximum(self.daq_gen_start_stop_max[_key])
        #         _gen_start_stop_spinbox.setValue(self.daq_gen_start_stop_values[_key])
        #         _gen_start_stop_spinbox.setStyleSheet(self.common_spinbox_format)
        #         _gen_start_stop_spinbox.setFixedWidth(self.gen_config_spinbox_width)
        #         self.daq_gen_start_stop_Qt_objects[_key] = _gen_start_stop_spinbox
        #         _gen_start_stop_spinbox.valueChanged.connect(lambda value, _key=_key: H2GDAQ_components.on_daq_gen_start_stop_changed_spinbox(self, value, _key))
        #         _gen_start_stop_layout.addWidget(_gen_start_stop_spinbox, alignment=Qt.AlignRight)
        #     elif self.daq_gen_start_stop_UI_type[_key] == "checkbox":
        #         _gen_start_stop_checkbox = QCheckBox()
        #         _gen_start_stop_checkbox.setChecked(self.daq_gen_start_stop_values[_key])
        #         self.daq_gen_start_stop_Qt_objects[_key] = _gen_start_stop_checkbox
        #         _gen_start_stop_checkbox.stateChanged.connect(lambda state, _key=_key: H2GDAQ_components.on_daq_gen_start_stop_changed_checkbox(self, state == 2, _key))
        #         _gen_start_stop_layout.addWidget(_gen_start_stop_checkbox, alignment=Qt.AlignRight)
        
        # tabDAQ_L3_info_layout.addStretch(1)

        # tabDAQ_L3_daq_gen_start_stop_auto_layout = QHBoxLayout()
        # tabDAQ_L3_info_layout.addLayout(tabDAQ_L3_daq_gen_start_stop_auto_layout)
        # self.gen_start_stop_auto_select = QCheckBox(tabDAQ_L3_daq_gen_start_stop_auto_layout.widget())
        # self.gen_start_stop_auto_select.setChecked(True)
        # self.gen_start_stop_auto_select.clicked.connect(lambda: H2GDAQ_components.on_auto_daq_gen_start_stop_changed(self, status=self.gen_start_stop_auto_select.isChecked()))

        # gen_start_stop_auto_label = QLabel("Auto Sending Start/Stop")
        # gen_start_stop_auto_label.setStyleSheet(self.module_content_format)

        # tabDAQ_L3_daq_gen_start_stop_auto_layout.addWidget(gen_start_stop_auto_label, alignment=Qt.AlignLeft)
        # tabDAQ_L3_daq_gen_start_stop_auto_layout.addWidget(self.gen_start_stop_auto_select, alignment=Qt.AlignRight)

        # tabDAQ_L3_info_send_button = QPushButton("Send DAQ + Generator Settings")
        # tabDAQ_L3_info_send_button.setStyleSheet(self.common_button_format)
        
        # tabDAQ_L3_info_layout.addWidget(tabDAQ_L3_info_send_button)
        # tabDAQ_L3_info_send_button.clicked.connect(lambda: H2GDAQ_components.on_daq_gen_start_stop_send_button_pushed(self))

        self.tabDAQ_L3_daq_gen_daq_push = QPushButton("DAQ Push")
        self.tabDAQ_L3_daq_gen_daq_push.setStyleSheet(self.common_button_format)
        self.tabDAQ_L3_daq_gen_daq_push.setDisabled(True)
        self.tabDAQ_L3_daq_gen_daq_push.clicked.connect(lambda: H2GDAQ_components.on_daq_push_button_pushed(self))
        tabDAQ_L3_info_layout.addWidget(self.tabDAQ_L3_daq_gen_daq_push)

        self.tabDAQ_L3_daq_gen_start_gen = QPushButton("Start Generator")
        self.tabDAQ_L3_daq_gen_start_gen.setStyleSheet(self.common_button_format)
        self.tabDAQ_L3_daq_gen_start_gen.setDisabled(True)
        self.tabDAQ_L3_daq_gen_start_gen.clicked.connect(lambda: H2GDAQ_components.on_gen_start_button_pushed(self))
        tabDAQ_L3_info_layout.addWidget(self.tabDAQ_L3_daq_gen_start_gen)

        self.tabDAQ_L3_daq_gen_stop_gen = QPushButton("Stop Generator")
        self.tabDAQ_L3_daq_gen_stop_gen.setStyleSheet(self.common_button_format)
        self.tabDAQ_L3_daq_gen_stop_gen.setDisabled(True)
        self.tabDAQ_L3_daq_gen_stop_gen.clicked.connect(lambda: H2GDAQ_components.on_gen_stop_button_pushed(self))
        tabDAQ_L3_info_layout.addWidget(self.tabDAQ_L3_daq_gen_stop_gen)

        self.tabDAQ_L3_bottom_tabs = QTabWidget()
        self.tabDAQ_L3_bottom_tabs.setTabPosition(QTabWidget.North)
        tabDAQ_L3_layout.addWidget(self.tabDAQ_L3_bottom_tabs)

        tabDAQ_L3_HV_widget = QWidget(tabDAQ_L3_layout.widget())
        tabDAQ_L3_HV_layout = QVBoxLayout()
        tabDAQ_L3_HV_widget.setLayout(tabDAQ_L3_HV_layout)
        tabDAQ_L3_HV_widget.setStyleSheet(self.module_widget_background)
        self.tabDAQ_L3_bottom_tabs.addTab(tabDAQ_L3_HV_widget, "High Voltage")

        tabDAQ_L3_reset_adj_widget = QWidget(tabDAQ_L3_layout.widget())
        tabDAQ_L3_reset_adj_layout = QVBoxLayout()
        tabDAQ_L3_reset_adj_widget.setLayout(tabDAQ_L3_reset_adj_layout)
        tabDAQ_L3_reset_adj_widget.setStyleSheet(self.module_widget_background)
        self.tabDAQ_L3_bottom_tabs.addTab(tabDAQ_L3_reset_adj_widget, "Reset/Adj")

        for _key in self.reset_adj_label_str.keys():
            if "label" in _key:
                _label_content = self.reset_adj_label_str[_key]
                _label_content = '--- ' + _label_content + ' ---'
                _reset_adj_label = QLabel(_label_content, alignment=Qt.AlignCenter)
                _reset_adj_label.setStyleSheet(self.divider_label_format)
                tabDAQ_L3_reset_adj_layout.addWidget(_reset_adj_label)
                continue
            _reset_adj_layout = QHBoxLayout()
            tabDAQ_L3_reset_adj_layout.addLayout(_reset_adj_layout)

            _reset_adj_label = QLabel(self.reset_adj_label_str[_key], alignment=Qt.AlignLeft)
            _reset_adj_label.setStyleSheet(self.module_content_format)
            _reset_adj_layout.addWidget(_reset_adj_label)

            if self.reset_adj_UI_type[_key] == "spinbox":
                _reset_adj_spinbox = QSpinBox()
                _reset_adj_spinbox.setMinimum(0)
                _reset_adj_spinbox.setMaximum(self.reset_adj_max[_key])
                _reset_adj_spinbox.setValue(self.reset_adj_values[_key])
                _reset_adj_spinbox.setStyleSheet(self.common_spinbox_format)
                _reset_adj_spinbox.setFixedWidth(self.gen_config_spinbox_width)
                self.reset_adj_Qt_objects[_key] = _reset_adj_spinbox
                _reset_adj_spinbox.valueChanged.connect(lambda value, _key=_key: H2GDAQ_components.on_reset_adj_changed_spinbox(self, value, _key))
                _reset_adj_layout.addWidget(_reset_adj_spinbox, alignment=Qt.AlignRight)
            elif self.reset_adj_UI_type[_key] == "checkbox":
                _reset_adj_checkbox = QCheckBox()
                _reset_adj_checkbox.setChecked(self.reset_adj_values[_key])
                self.reset_adj_Qt_objects[_key] = _reset_adj_checkbox
                _reset_adj_checkbox.stateChanged.connect(lambda state, _key=_key: H2GDAQ_components.on_reset_adj_changed_checkbox(self, state == 2, _key))
                _reset_adj_layout.addWidget(_reset_adj_checkbox, alignment=Qt.AlignRight)

        tabDAQ_L3_reset_adj_layout.addStretch(1)
        self.tabDAQ_L3_reset_adj_send_button = QPushButton("Send Reset/Adj Settings")
        self.tabDAQ_L3_reset_adj_send_button.setStyleSheet(self.common_button_format)
        tabDAQ_L3_reset_adj_layout.addWidget(self.tabDAQ_L3_reset_adj_send_button)
        self.tabDAQ_L3_reset_adj_send_button.clicked.connect(lambda: self.on_reset_adj_send_button_pushed())
        
        # tabDAQ_L3_HV_title = QLabel("High Voltage", alignment=Qt.AlignLeft)
        # tabDAQ_L3_HV_title.setStyleSheet(self.module_title_format)
        # tabDAQ_L3_HV_layout.addWidget(tabDAQ_L3_HV_title)

        tabDAQ_L3_HV_box_widget = QWidget(tabDAQ_L3_HV_layout.widget())
        tabDAQ_L3_HV_box_layout = QGridLayout()
        tabDAQ_L3_HV_box_widget.setLayout(tabDAQ_L3_HV_box_layout)
        tabDAQ_L3_HV_layout.addWidget(tabDAQ_L3_HV_box_widget)

        asic_box_per_line = 4
        asic_line_number = self.num_asic_tabs // asic_box_per_line + 1
        for _fpga in range(self.num_fpga_tabs):
            _fpga_checkbox = QCheckBox(f"F{_fpga}")
            _fpga_checkbox.setChecked(False)
            _fpga_checkbox.setStyleSheet('QCheckBox {font-size: 12px; font-family: "DejaVu Sans Mono"; color: #404040;} QCheckBox:disabled {color: #808080;}')
            self.hv_fpga_checkboxes.append(_fpga_checkbox)
            tabDAQ_L3_HV_box_layout.addWidget(_fpga_checkbox, _fpga * asic_line_number + 1, 0)
            _fpga_checkbox.stateChanged.connect(lambda state, _fpga=_fpga: H2GDAQ_components.on_HV_fpga_box_changed(self, state == 2, _fpga))
            for _asic in range(self.num_asic_tabs):
                _asic_checkbox = QCheckBox(f"A{_asic}")
                _asic_checkbox.setChecked(False)
                _asic_checkbox.setStyleSheet('QCheckBox {font-size: 12px; font-family: "DejaVu Sans Mono"; color: #404040;} QCheckBox:disabled {color: #808080;}')
                self.hv_checkboxes.append(_asic_checkbox)
                tabDAQ_L3_HV_box_layout.addWidget(_asic_checkbox, _fpga * asic_line_number + (_asic // asic_box_per_line + 1), _asic % asic_box_per_line + 1)
                _asic_checkbox.stateChanged.connect(lambda state, _fpga=_fpga, _asic=_asic: H2GDAQ_components.on_HV_asic_box_changed(self, state == 2, _fpga, _asic))

        # for _board_id in [208, 209, 210]:
        #     _tab_title = f"Board {_board_id}"
        #     _tab_widget = QTableWidget(30, 3)
        #     _tab_widget.setStyleSheet(self.status_sheet_background)
        #     # set column width
        #     _tab_widget.setColumnWidth(0, 50)
        #     _tab_widget.setColumnWidth(1, 150)
        #     _tab_widget.setColumnWidth(2, 150)
        #     # set row height
        #     _tab_widget.verticalHeader().setDefaultSectionSize(25)
        #     _tab_widget.setHorizontalHeaderLabels(["#", "Status", "Value"])
        #     # hide the vertical header
        #     _tab_widget.verticalHeader().setVisible(False)
        #     for _row in range(30):
        #         _tab_widget_number = QTableWidgetItem(str(self.status_sheet_list[_row]))
        #         _tab_widget_number.setFlags(Qt.ItemIsEnabled)
        #         _tab_widget_number.setTextAlignment(Qt.AlignCenter)
        #         _tab_widget.setItem(_row, 0, _tab_widget_number)

        #         _tab_widget_label = QTableWidgetItem(self.status_item_label[_row])
        #         _tab_widget_label.setFlags(Qt.ItemIsEnabled)
        #         _tab_widget_label.setTextAlignment(Qt.AlignCenter)
        #         _tab_widget.setItem(_row, 1, _tab_widget_label)
        #     tabDAQ_L3_info_tabs.addTab(_tab_widget, _tab_title)


        tabDAQ_L3_HV_layout.addStretch(1)
        self.tabDAQ_L3_HV_send_button = QPushButton("Send HV Settings")
        self.tabDAQ_L3_HV_send_button.setStyleSheet(self.common_button_format)
        tabDAQ_L3_HV_layout.addWidget(self.tabDAQ_L3_HV_send_button)
        self.tabDAQ_L3_HV_send_button.clicked.connect(lambda: H2GDAQ_components.on_HV_send_button_pushed(self))


        self.statusbar = QStatusBar()
        self.statusbar.setFixedHeight(30)
        self.setStatusBar(self.statusbar)

    def closeEvent(self, event):
        if self.stop_DAQ_flag == False:
            # choose wether to exit or not
            reply = QMessageBox.question(self, 'Exit Application', 'Are you sure you want to exit the application while data acquisition is in progress?', QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if reply == QMessageBox.No:
                event.ignore()
                return
            else:
                H2GDAQ_components.on_stop_DAQ(self)
        self.saveIniConfig()
        super().closeEvent(event)

    def loadIniConfig(self):
        config = configparser.ConfigParser()
        config.read(self.config_file)

        mode_sel = config.get('Initial Settings', 'DAQ_mode', fallback='manual')
        if mode_sel == "manual":
            self.on_manual_mode_selected()
        elif mode_sel == "timed":
            self.on_timed_mode_selected()
        elif mode_sel == "event":
            self.on_event_mode_selected()
        else:
            logger.error("Unknown DAQ mode selected")

        last_working_folder = config.get('Initial Settings', 'working_folder', fallback=os.getcwd())
        self.manual_workingfolder_set(last_working_folder)

        last_event_cnt = config.getint('Initial Settings', 'DAQ_event_cnt', fallback=0)
        self.on_event_cnt_changed(last_event_cnt)

        last_timer_sec = config.getint('Initial Settings', 'DAQ_timer_sec', fallback=0)
        self.on_timed_sec_changed(last_timer_sec)

        last_run_number = config.getint('Initial Settings', 'DAQ_run_number', fallback=0)
        self.on_runnumber_changed(last_run_number)

        last_auto_daq_gen = config.getboolean('Initial Settings', 'auto_daq_gen', fallback=False)
        last_auto_reset_cnt = config.getboolean('Initial Settings', 'auto_reset_cnt', fallback=False)


        H2GDAQ_components.on_gen_settings_auto_select_checkbox_changed(self, last_auto_daq_gen)
        H2GDAQ_components.on_reset_cnt_auto_select_checkbox_changed(self, last_auto_reset_cnt)

        # online monitoring
        last_run_monitoring = config.getboolean('Initial Settings', 'run_monitoring', fallback=False)
        H2GDAQ_components.on_run_monitoring_checkbox_changed(self, last_run_monitoring)

        last_monitoring_executable = config.get('Initial Settings', 'monitoring_executable', fallback=self.monitoring_executable)
        if os.path.exists(last_monitoring_executable):
            self.on_load_monitoring_executable(last_monitoring_executable)

        # last_auto_daq_gen = config.getboolean('Initial Settings', 'auto_daq_gen', fallback=False)
        # H2GDAQ_components.on_auto_daq_gen_start_stop_changed(self, last_auto_daq_gen)

        for key in self.gen_setting_values.keys():
            if config.has_option('Initial Settings', key):
                if self.gen_settings_UI_type[key] == "spinbox":
                    self.gen_setting_values[key] = config.getint('Initial Settings', key)
                    H2GDAQ_components.on_gen_setting_changed_spinbox(self, self.gen_setting_values[key], key)
                elif self.gen_settings_UI_type[key] == "checkbox":
                    self.gen_setting_values[key] = config.getboolean('Initial Settings', key)
                    H2GDAQ_components.on_gen_setting_changed_checkbox(self, self.gen_setting_values[key] == 1, key)
                elif self.gen_settings_UI_type[key] == "textinput":
                    self.gen_setting_values[key] = config.getint('Initial Settings', key)
                    H2GDAQ_components.on_gen_setting_changed_lineedit(self, str(self.gen_setting_values[key]), key)
            else:
                logger.info(f"Key {key} not found in config file")

        for _fpga in range(self.num_fpga_tabs):
            for _asic in range(self.num_asic_tabs):
                _hv_key = f"FPGA{_fpga}_ASIC{_asic}"
                if config.has_option('HV Settings', _hv_key):
                    self.hv_settings[_fpga * self.num_asic_tabs + _asic] = config.getboolean('HV Settings', _hv_key)
                    H2GDAQ_components.on_HV_asic_box_changed(self, self.hv_settings[_fpga * self.num_asic_tabs + _asic], _fpga, _asic)
                else:
                    logger.info(f"Key {_hv_key} not found in config file")

        for key in self.reset_adj_values.keys():
            if config.has_option('Initial Settings', key):
                if self.reset_adj_UI_type[key] == "spinbox":
                    self.reset_adj_values[key] = config.getint('Initial Settings', key)
                    H2GDAQ_components.on_reset_adj_changed_spinbox(self, self.reset_adj_values[key], key)
                elif self.reset_adj_UI_type[key] == "checkbox":
                    self.reset_adj_values[key] = config.getboolean('Initial Settings', key)
                    H2GDAQ_components.on_reset_adj_changed_checkbox(self, self.reset_adj_values[key] == 1, key)
            else:
                logger.info(f"Key {key} not found in config file")

        l2_tab_sel = config.getint('Initial Settings', 'selected_l2_tab', fallback=0)
        self.tabDAQ_L2_tabs.setCurrentIndex(l2_tab_sel)
        l3_bottom_tab_sel = config.getint('Initial Settings', 'selected_l3_bottom_tab', fallback=0)
        self.tabDAQ_L3_bottom_tabs.setCurrentIndex(l3_bottom_tab_sel)

        self.is_muted = config.getboolean('Initial Settings', 'muted', fallback=False)
        if self.is_muted == True:
            self.mute_button.setIcon(QIcon(self.mute_icon_unmute))
        else:
            self.mute_button.setIcon(QIcon(self.mute_icon_mute))


    def saveIniConfig(self):
        config = configparser.ConfigParser()
        config.read(self.config_file)
        if not config.has_section('Initial Settings'):
            config.add_section('Initial Settings')

        config.set('Initial Settings', 'working_folder', self.selected_working_folder)

        if self.current_DAQ_mode == "manual":
            config.set('Initial Settings', 'DAQ_mode', 'manual')
        elif self.current_DAQ_mode == "timed":
            config.set('Initial Settings', 'DAQ_mode', 'timed')
        elif self.current_DAQ_mode == "event":
            config.set('Initial Settings', 'DAQ_mode', 'event')

        config.set('Initial Settings', 'DAQ_event_cnt', str(self.DAQ_event_cnt))
        config.set('Initial Settings', 'DAQ_timer_sec', str(self.DAQ_timer_sec))
        config.set('Initial Settings', 'DAQ_run_number', str(self.current_RunNumber))

        config.set('Initial Settings', 'auto_daq_gen', str(self.send_gen_before_DAQ))
        config.set('Initial Settings', 'auto_reset_cnt', str(self.reset_cnt_before_DAQ))

        # save online monitoring settings
        config.set('Initial Settings', 'run_monitoring', str(self.run_online_monitoring))
        config.set('Initial Settings', 'monitoring_executable', self.monitoring_executable)

        # config.set('Initial Settings', 'auto_daq_gen', str(self.gen_start_stop_auto_select.isChecked()))

        if not config.has_section('HV Settings'):
            config.add_section('HV Settings')

        for _fpga in range(self.num_fpga_tabs):
            for _asic in range(self.num_asic_tabs):
                _hv_key = f"FPGA{_fpga}_ASIC{_asic}"
                _hv_value = self.hv_settings[_fpga * self.num_asic_tabs + _asic]
                config.set('HV Settings', _hv_key, str(_hv_value))

        # save all key and contents in gen_setting_values
        for key in self.gen_setting_values.keys():
            config.set('Initial Settings', key, str(self.gen_setting_values[key]))

        for key in self.reset_adj_values.keys():
            config.set('Initial Settings', key, str(self.reset_adj_values[key]))

        # for key in self.daq_gen_start_stop_values.keys():
        #     config.set('Initial Settings', key, str(self.daq_gen_start_stop_values[key]))

        config.set('Initial Settings', 'selected_l2_tab', str(self.tabDAQ_L2_tabs.currentIndex()))
        config.set('Initial Settings', 'selected_l3_bottom_tab', str(self.tabDAQ_L3_bottom_tabs.currentIndex()))

        # save mute settings
        config.set('Initial Settings', 'muted', str(self.is_muted))

        with open(self.config_file, 'w') as configfile:
            config.write(configfile)
        

    def manual_workingfolder_set(self, _folder):
        self.selected_working_folder = _folder
        self.tabDAQ_L1_workingfolder_workingfolder.setText(self.selected_working_folder)

    def on_workingfolder_select(self):
        _folder_sel_dialog = QFileDialog()
        _folder_sel_dialog.setFileMode(QFileDialog.Directory)
        _folder_sel_dialog.setOption(QFileDialog.ShowDirsOnly, True)

        if _folder_sel_dialog.exec():
            self.selected_working_folder = _folder_sel_dialog.selectedFiles()[0]
            # connect to the working folder label
            self.tabDAQ_L1_workingfolder_workingfolder.setText(self.selected_working_folder)

    def on_set_monitoring_executable(self):
        monitoring = QFileDialog()
        monitoring.setFileMode(QFileDialog.Directory)
        monitoring.setOption(QFileDialog.ShowDirsOnly, True)

        if monitoring.exec():
            self.monitoring_executable = monitoring.selectedFiles()[0]
            # connect to the working folder label
            self.tabDAQ_L1_monitoring_executable_path.setText(self.monitoring_executable)

    def on_load_monitoring_executable(self, _folder):
        self.monitoring_executable = _folder
        self.tabDAQ_L1_monitoring_executable_path.setText(self.monitoring_executable)

    def on_iodelay_button_pushed(self):
        # ask for confirmation
        reply = QMessageBox.question(self, 'Set IODELAY', 'Are you sure you want to set the IODELAY?\nIt will turn off the RunL/R in Top Reg\nand do a soft reset', QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.No:
            return
        # open a dialog to set the IODELAY
        mainwindow_pos = self.pos()
        mainwindow_pos.setX(mainwindow_pos.x() + self.width() / 2)
        mainwindow_pos.setY(mainwindow_pos.y() + self.height() / 2)
        H2GConfig_windows.IODelaySettingDialog(self, self.num_fpga_tabs, mainwindow_pos).exec()

    def on_workingfolder_open(self):
        path = self.selected_working_folder
        if sys.platform == 'win32': # Windows
            subprocess.run(['explorer', path], check=True)
        elif sys.platform == 'darwin': # macOS
            subprocess.run(['open', path], check=True)
        elif sys.platform == 'linux':
            QMessageBox.warning(self, 'Open Folder', 'Opening folder is not supported on Linux', QMessageBox.Ok, QMessageBox.Ok)
        else:
            logging.error(f'Unsupported platform: {sys.platform}')

    def on_save_options(self):
        if self.tabDAQ_L1_workingfolder_save_options_button.text() == "Print to Terminal":
            # ask for confirmation
            reply = QMessageBox.question(self, 'Print to Terminal', 'Are you sure you want to print the data to terminal?\nIt is only for slow rate debugging', QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if reply == QMessageBox.No:
                return
            self.tabDAQ_L1_workingfolder_save_options_button.setText("  Save to File   ")
            self.tabDAQ_L1_workingfolder_workingfolder.setStyleSheet("font-weight: normal; font-family: 'DejaVu Sans Mono'; font-size: 13px; color: #e0e0e0;")
            self.data_save_to_file = False
        elif self.tabDAQ_L1_workingfolder_save_options_button.text() == "  Save to File   ":
            self.tabDAQ_L1_workingfolder_save_options_button.setText("Print to Terminal")
            self.tabDAQ_L1_workingfolder_workingfolder.setStyleSheet(self.module_content_format)
            self.data_save_to_file = True

    def on_manual_mode_selected(self):
        self.current_DAQ_mode = "manual"
        if not self.manual_select.isChecked():
            self.manual_select.setChecked(True)
        self.event_cnt_select.setChecked(False)
        self.time_cnt_select.setChecked(False)

    def on_timed_mode_selected(self):
        self.current_DAQ_mode = "timed"
        self.manual_select.setChecked(False)
        self.event_cnt_select.setChecked(False)
        if not self.time_cnt_select.isChecked():
            self.time_cnt_select.setChecked(True)

    def on_event_mode_selected(self):
        self.current_DAQ_mode = "event"
        self.manual_select.setChecked(False)
        if not self.event_cnt_select.isChecked():
            self.event_cnt_select.setChecked(True)
        self.time_cnt_select.setChecked(False)

    def on_timed_sec_changed(self, value):
        self.DAQ_timer_sec = value
        if value != self.time_cnt_box.value():
            self.time_cnt_box.setValue(value)
    
    def on_event_cnt_changed(self, value):
        self.DAQ_event_cnt = value
        if value != self.event_cnt_box.value():
            self.event_cnt_box.setValue(value)

    def on_runnumber_changed(self, value):
        self.current_RunNumber = value
        if value != self.runnum_box.value():
            self.runnum_box.setValue(value)

    # def on_phasescan_button_pushed(self):
    #     # Original manual phase scan code.
    #     for _phase_value in range(16):
    #         self.worker.send_phase_settings(self, _phase_value)
    #         time.sleep(0.5)

    def on_auto_phase_scan_button_pushed(self):
        self.auto_phase_scan_active = True
        threading.Thread(target=self.auto_phase_scan, daemon=True).start()

    def auto_phase_scan(self):
        for phase in range(16):
            if not self.auto_phase_scan_active:
                break
            # Set current phase.
            self.worker.send_phase_settings(self, phase)
            # Allow the new phase to settle.
            time.sleep(0.5)
            # Start DAQ automatically.
            QMetaObject.invokeMethod(self.DAQ_Start_button, "click", Qt.QueuedConnection)
            # Wait for acquisition to complete (adjust wait time as needed)
            time.sleep(0.5)
            while(self.stop_DAQ_flag == False):
                if not self.auto_phase_scan_active:
                    break
                time.sleep(1)
            # Stop DAQ.
            # QMetaObject.invokeMethod(self.DAQ_Stop_button, "click", Qt.QueuedConnection)
            # H2GDAQ_components.on_stop_DAQ_phase_scan(self)
            # Slight pause before next phase.
            time.sleep(0.5)
        self.auto_phase_scan_active = False  # scan complete

    def on_request_debug_data_button_pushed(self):
        self.debug_sel_fpga = 0
        self.debug_sel_asic = 0
        mainwindow_pos = self.pos()
        mainwindow_pos.setX(mainwindow_pos.x() + self.width() / 2)
        mainwindow_pos.setY(mainwindow_pos.y() + self.height() / 2)
        selection_dialog = H2GConfig_windows.ASICSelectionDialog(self, mainwindow_pos)
        if selection_dialog.exec() == QDialog.Accepted:
            # logger.debug(f"Selected FPGA: {self.debug_sel_fpga}, ASIC: {self.debug_sel_asic}")
            pass

        debug_data = self.worker.get_debug_data(self, self.debug_sel_fpga, self.debug_sel_asic)

        # logger.debug(f"Debug data: {debug_data}")

        if debug_data is None:
            QMessageBox.warning(self, 'Request Debug Data', 'Failed to get debug data', QMessageBox.Ok, QMessageBox.Ok)
            return
        else:
            self.debug_table.clearContents()
            for _row in range(20):
                _tab_widget_label = QTableWidgetItem(self.debug_table_item[_row])
                _tab_widget_label.setFlags(Qt.ItemIsEnabled)
                _tab_widget_label.setTextAlignment(Qt.AlignCenter)
                self.debug_table.setItem(_row, 0, _tab_widget_label)
                if _row == 0:
                    _value = debug_data['header']
                elif _row == 1:
                    _value = debug_data['fpga_address']
                elif _row == 2:
                    _value = debug_data['packet_type']
                elif _row == 3:
                    _value = debug_data['bx_counter']
                elif _row == 4:
                    _value = debug_data['s_io_dlyo_daq1']
                elif _row == 5:
                    _value = debug_data['s_io_dlyo_daq0']
                elif _row == 6:
                    _value = debug_data['adj_ready']
                elif _row == 7:
                    _value = debug_data['adj_dq1']
                elif _row == 8:
                    _value = debug_data['adj_dq0']
                elif _row == 9:
                    _value = debug_data['adj_tr3']
                elif _row == 10:
                    _value = debug_data['adj_tr2']
                elif _row == 11:
                    _value = debug_data['adj_tr1']
                elif _row == 12:
                    _value = debug_data['adj_tr0']
                elif _row == 13:
                    _value = debug_data['adj_error']
                elif _row == 14:
                    _value = debug_data['trg0_value']
                elif _row == 15:
                    _value = debug_data['trg1_value']
                elif _row == 16:
                    _value = debug_data['trg2_value']
                elif _row == 17:
                    _value = debug_data['trg3_value']
                elif _row == 18:
                    _value = debug_data['data0_value']
                elif _row == 19:
                    _value = debug_data['data1_value']
                else:
                    _value = 0
                _value = hex(int(_value))
                _tab_widget_value = QTableWidgetItem(_value)
                _tab_widget_value.setFlags(Qt.ItemIsEnabled)
                _tab_widget_value.setTextAlignment(Qt.AlignCenter)
                self.debug_table.setItem(_row, 1, _tab_widget_value)

    def on_reset_adj_send_button_pushed(self):
        self.reset_sel_fpga = 0
        mainwindow_pos = self.pos()
        mainwindow_pos.setX(mainwindow_pos.x() + self.width() / 2)
        mainwindow_pos.setY(mainwindow_pos.y() + self.height() / 2)

        if self.num_fpga_tabs > 1:
            selection_dialog = H2GConfig_windows.FPGASelectionDialog(self, mainwindow_pos)
            if selection_dialog.exec() == QDialog.Accepted:
                # logger.debug(f"Selected FPGA: {self.reset_sel_fpga}")
                pass
        else:
            self.reset_sel_fpga = 0
        
        self.worker.send_reset_adj_settings(self, self.reset_sel_fpga)

def run_application():
    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon(os.path.join('asset', 'icon_h2gdaq.svg')))
    app.setStyle('Fusion')
    app.setApplicationName('H2GDAQ')
    dialog = H2GConfig_windows.InitialSystemDialog('config.ini', _version = software_version)
    if dialog.exec():
        system_number_info = dialog.getSystemNumber()
        main_window = MainWindow(system_number_info["fpga"], system_number_info["asic"], 'config.ini')
        main_window.show()
        sys.exit(app.exec())

if __name__ == '__main__':
    run_application()