from PySide6.QtWidgets import QApplication, QMainWindow, QDialog, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QPushButton, QTabWidget, QWidget, QSpinBox, QTabBar, QTableWidgetItem, QTableWidget, QGridLayout, QScrollArea, QHeaderView, QCheckBox, QProgressBar, QMessageBox, QFileDialog
from PySide6.QtCore import Qt
from PySide6.QtGui import QPixmap
import sys, os, time
import configparser, json
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
import H2GConfig_windows
from loguru import logger
import subprocess
import signal
import time
import finishing_sound

from PySide6.QtGui import QFont, QIcon

log_format = "<level>{time:YYYY-MM-DD HH:mm:ss} | {level:.1s} | {message}</level>"
logger.remove()  # Remove default handler
logger.add(sink=sys.stderr, format=log_format)

def write_data_file_header(self, _file_path):

    with open(_file_path, 'w') as file:
        file.write("##################################################\n")
        file.write("# KCU-H2GCROC Data File\n")
        file.write("# File Version: " + self.version + "\n")
        file.write("# Run Number: " + str(self.current_RunNumber) + "\n")
        current_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        file.write("# Date and Time: " + current_time + "\n")
        file.write("# Data Acquisition Mode: " + self.current_DAQ_mode + "\n")
        if self.current_DAQ_mode == "timed":
            file.write("# Data Acquisition Time: " + str(self.DAQ_timer_sec) + " seconds\n")
        for key in self.gen_setting_values.keys():
            file.write("# Generator Setting " + key + ": " + str(self.gen_setting_values[key]) + "\n")
        file.write("##################################################\n")

def on_start_DAQ(self):
    _daq_push       = self.daq_gen_start_stop_values['daq_push']
    _gen_start_stop = self.daq_gen_start_stop_values['generator_start_stop']
    _daq_start_stop = self.daq_gen_start_stop_values['daq_start_stop']

    self.stop_DAQ_flag = False
    self.DAQ_completed_flag = False
    self.DAQ_Stop_button.setEnabled(True)
    self.DAQ_Start_button.setEnabled(False)
    self.tabDAQ_L3_HV_send_button.setEnabled(False)
    self.tabDAQ_L3_reset_adj_send_button.setEnabled(False)
    self.tabDAQ_L2_debug_request_button.setEnabled(False)

    self.tabDAQ_L3_daq_gen_daq_push.setEnabled(True)
    self.tabDAQ_L3_daq_gen_start_gen.setEnabled(True)

    # * --- Send reset pack counter command ---
    if self.reset_cnt_before_DAQ == True:
        self.worker.send_reset_pack_counter(self)

    # ! === Manual mode DAQ ===
    if self.current_DAQ_mode == "manual":
        if self.data_save_to_file == True:
            full_file_path = str(self.current_RunNumber)
            if len(full_file_path) < 3:
                full_file_path = "0"*(3-len(full_file_path)) + full_file_path
            full_file_path = self.selected_working_folder + "/Run" + full_file_path + ".h2g"
            # if the file already exists, ask for confirmation
            if os.path.exists(full_file_path):
                reply = QMessageBox.question(self, "File Exists", "The file already exists. Overwrite?", QMessageBox.Yes | QMessageBox.No)
                if reply == QMessageBox.No:
                    self.DAQ_Stop_button.setEnabled(False)
                    self.DAQ_Start_button.setEnabled(True)
                    return
                
            write_data_file_header(self, full_file_path)

            # Now that the file exists, we can start the monitoring if necessary
            if self.run_online_monitoring == True:
                start_monitoring(self)


            busy_progress = QProgressBar()
            busy_progress.setRange(0, 0)
            busy_progress.setFixedSize(200, 20)
            busy_progress.setFormat("Busy")
            self.statusbar.addPermanentWidget(busy_progress)
            self.statusbar.showMessage("Data acquisition in progress")

            self.worker.start_datataking_process(self, full_file_path)

            while not self.stop_DAQ_flag:
                QApplication.processEvents()

            busy_progress.deleteLater()
            self.statusbar.clearMessage()

            self.worker.stop_datataking_process(self)
        else:
            busy_progress = QProgressBar()
            busy_progress.setRange(0, 0)
            busy_progress.setFixedSize(200, 20)
            busy_progress.setFormat("Busy")
            self.statusbar.addPermanentWidget(busy_progress)
            self.statusbar.showMessage("Data acquisition in progress")

            self.worker.start_datataking_process_terminal(self)

            while not self.stop_DAQ_flag:
                QApplication.processEvents()

            busy_progress.deleteLater()
            self.statusbar.clearMessage()

            self.worker.stop_datataking_process(self)

    # ! === Timed mode DAQ ===
    elif self.current_DAQ_mode == "timed":
        if self.data_save_to_file == True:
            full_file_path = str(self.current_RunNumber)
            if len(full_file_path) < 3:
                full_file_path = "0"*(3-len(full_file_path)) + full_file_path
            full_file_path = self.selected_working_folder + "/Run" + full_file_path + ".h2g"

            if os.path.exists(full_file_path):
                reply = QMessageBox.question(self, "File Exists", "The file already exists. Overwrite?", QMessageBox.Yes | QMessageBox.No)
                if reply == QMessageBox.No:
                    self.DAQ_Stop_button.setEnabled(False)
                    self.DAQ_Start_button.setEnabled(True)
                    return
                
            write_data_file_header(self, full_file_path)

            # Now that the file exists, we can start the monitoring if necessary
            if self.run_online_monitoring == True:
                start_monitoring(self)

            timer_progress = QProgressBar()
            timer_progress.setRange(0, 100)
            timer_progress.setFixedSize(200, 20)
            timer_progress.setFormat("Time used: %p%")
            # show percentage
            timer_progress.setValue(0)

            self.statusbar.addPermanentWidget(timer_progress)
            self.statusbar.showMessage("Data acquisition in progress")

            self.stop_DAQ_lambda = lambda: on_stop_DAQ_phase_scan(self)
            self.set_progress_lambda = lambda _value: timer_progress.setValue(_value)

            self.worker.daq_timeout_signal.connect(self.stop_DAQ_lambda)
            self.worker.timer_update_signal.connect(self.set_progress_lambda)

            self.worker.start_datataking_process_timed(self, full_file_path, self.DAQ_timer_sec) 

            while not self.stop_DAQ_flag:
                QApplication.processEvents()

            self.worker.timer_update_signal.disconnect(self.set_progress_lambda)
            self.worker.daq_timeout_signal.disconnect(self.stop_DAQ_lambda)
            self.worker.stop_datataking_process(self)

            if timer_progress.value() < 99:
                QMessageBox.warning(self, "DAQ Interrupted", "Data acquisition was interrupted before the timer expired", QMessageBox.Ok)

            timer_progress.deleteLater()
            self.statusbar.clearMessage()
        else:
            timer_progress = QProgressBar()
            timer_progress.setRange(0, 100)
            timer_progress.setFixedSize(200, 20)
            timer_progress.setFormat("Time used: %p%")
            # show percentage
            timer_progress.setValue(0)

            self.statusbar.addPermanentWidget(timer_progress)
            self.statusbar.showMessage("Data acquisition in progress")

            self.stop_DAQ_lambda = lambda: on_stop_DAQ_phase_scan(self)
            self.set_progress_lambda = lambda _value: timer_progress.setValue(_value)

            self.worker.daq_timeout_signal.connect(self.stop_DAQ_lambda)
            self.worker.timer_update_signal.connect(self.set_progress_lambda)

            self.worker.start_datataking_process_timed_terminal(self, self.DAQ_timer_sec) 

            while not self.stop_DAQ_flag:
                QApplication.processEvents()

            self.worker.timer_update_signal.disconnect(self.set_progress_lambda)
            self.worker.daq_timeout_signal.disconnect(self.stop_DAQ_lambda)
            self.worker.stop_datataking_process(self)

            timer_progress.deleteLater()
            self.statusbar.clearMessage()

    # ! === Event mode DAQ ===
    elif self.current_DAQ_mode == "event":
        if self.data_save_to_file == True:
            full_file_path = str(self.current_RunNumber)
            if len(full_file_path) < 3:
                full_file_path = "0"*(3-len(full_file_path)) + full_file_path
            full_file_path = self.selected_working_folder + "/Run" + full_file_path + ".h2g"

            if os.path.exists(full_file_path):
                reply = QMessageBox.question(self, "File Exists", "The file already exists. Overwrite?", QMessageBox.Yes | QMessageBox.No)
                if reply == QMessageBox.No:
                    self.DAQ_Stop_button.setEnabled(False)
                    self.DAQ_Start_button.setEnabled(True)
                    return
                
            write_data_file_header(self, full_file_path)

            # Now that the file exists, we can start the monitoring if necessary
            if self.run_online_monitoring == True:
                start_monitoring(self)

            event_progress = QProgressBar()
            event_progress.setRange(0, 100)
            event_progress.setFixedSize(200, 20)
            event_progress.setFormat("Events collected: %v%")

            event_progress.setValue(0)

            self.statusbar.addPermanentWidget(event_progress)
            self.statusbar.showMessage("Data acquisition in progress")

            self.stop_DAQ_lambda = lambda: on_stop_DAQ_phase_scan(self)
            self.set_progress_lambda = lambda _value: event_progress.setValue(_value)

            self.worker.daq_event_timeout_signal.connect(self.stop_DAQ_lambda)
            self.worker.event_update_signal.connect(self.set_progress_lambda)

            self.worker.start_datataking_process_event(self, full_file_path, self.DAQ_event_cnt)

            while not self.stop_DAQ_flag:
                QApplication.processEvents()

            self.worker.event_update_signal.disconnect(self.set_progress_lambda)
            self.worker.daq_event_timeout_signal.disconnect(self.stop_DAQ_lambda)
            self.worker.stop_datataking_process(self)

            if self.DAQ_completed_flag == False:
                QMessageBox.warning(self, "DAQ Interrupted", "Data acquisition was interrupted before the event count was reached", QMessageBox.Ok)

            event_progress.deleteLater()
            self.statusbar.clearMessage()
        else:
            event_progress = QProgressBar()
            event_progress.setRange(0, 100)
            event_progress.setFixedSize(200, 20)
            event_progress.setFormat("Events collected: %v%")

            event_progress.setValue(0)

            self.statusbar.addPermanentWidget(event_progress)
            self.statusbar.showMessage("Data acquisition in progress")

            self.stop_DAQ_lambda = lambda: on_stop_DAQ_phase_scan(self)
            self.set_progress_lambda = lambda _value: event_progress.setValue(_value)

            self.worker.daq_event_timeout_signal.connect(self.stop_DAQ_lambda)
            self.worker.event_update_signal.connect(self.set_progress_lambda)

            self.worker.start_datataking_process_event_terminal(self, self.DAQ_event_cnt)

            while not self.stop_DAQ_flag:
                QApplication.processEvents()

            self.worker.event_update_signal.disconnect(self.set_progress_lambda)
            self.worker.daq_event_timeout_signal.disconnect(self.stop_DAQ_lambda)
            self.worker.stop_datataking_process(self)

            event_progress.deleteLater()
            self.statusbar.clearMessage()
    else:
        logger.critical("Invalid DAQ mode selected")

def on_stop_DAQ_core(self):
    self.stop_DAQ_flag = True
    self.DAQ_Stop_button.setEnabled(False)
    self.DAQ_Start_button.setEnabled(True)
    self.tabDAQ_L3_daq_gen_daq_push.setEnabled(False)
    self.tabDAQ_L3_daq_gen_start_gen.setEnabled(False)
    self.tabDAQ_L3_daq_gen_stop_gen.setEnabled(False)
    self.tabDAQ_L3_HV_send_button.setEnabled(True)
    self.tabDAQ_L3_reset_adj_send_button.setEnabled(True)
    self.tabDAQ_L2_debug_request_button.setEnabled(True)

    stop_monitoring(self)

def on_stop_DAQ(self):
    on_stop_DAQ_phase_scan(self)

    self.auto_phase_scan_active = False  # added to cancel auto phase scan

def on_stop_DAQ_phase_scan(self):
    on_stop_DAQ_core(self)

    self.current_RunNumber += 1
    self.runnum_box.setValue(self.current_RunNumber)

    if self.current_DAQ_mode in ("timed", "event"):
        if self.is_muted == False:  
            finishing_sound.play_finishing_sound()

def on_daq_push_button_pushed(self):
    self.worker.send_daq_push(self)
    self.tabDAQ_L3_daq_gen_start_gen.setEnabled(True)
    self.tabDAQ_L3_daq_gen_stop_gen.setEnabled(False)

def on_gen_start_button_pushed(self):
    self.worker.send_gen_start(self)
    self.tabDAQ_L3_daq_gen_start_gen.setEnabled(False)
    self.tabDAQ_L3_daq_gen_stop_gen.setEnabled(True)

def on_gen_stop_button_pushed(self):
    self.worker.send_gen_stop(self)
    self.tabDAQ_L3_daq_gen_start_gen.setEnabled(True)
    self.tabDAQ_L3_daq_gen_stop_gen.setEnabled(False)

def on_gen_setting_changed_spinbox(self, _value, _setting):
    # print(f"Setting {_setting} changed to {_value}")
    self.gen_setting_values[_setting] = _value
    if self.gen_settings_Qt_objects[_setting].value() != _value:
        self.gen_settings_Qt_objects[_setting].setValue(_value)

def on_gen_setting_changed_checkbox(self, _state, _setting):
    # print(f"Setting {_setting} changed to {_state}")
    if _state == True:
        self.gen_setting_values[_setting] = 0x01
    else:
        self.gen_setting_values[_setting] = 0x00
    if self.gen_settings_Qt_objects[_setting].isChecked() != _state:
        self.gen_settings_Qt_objects[_setting].setChecked(_state)

def on_gen_setting_changed_lineedit(self, _text, _setting):
    # print(f"Setting {_setting} changed to {_text}")
    # if text is not a number, do not change the value
    try:
        long_number = int(_text)
    except ValueError:
        logger.warning(f"Invalid value entered for setting {_setting}")
        return
    self.gen_setting_values[_setting] = long_number
    if self.gen_settings_Qt_objects[_setting].text() != _text:
        self.gen_settings_Qt_objects[_setting].setText(_text)

def on_daq_gen_start_stop_changed_spinbox(self, _value, _setting):
    # print(f"Setting {_setting} changed to {_value}")
    self.daq_gen_start_stop_values[_setting] = _value
    if self.daq_gen_start_stop_Qt_objects[_setting].value() != _value:
        self.daq_gen_start_stop_Qt_objects[_setting].setValue(_value)

def on_daq_gen_start_stop_changed_checkbox(self, _state, _setting):
    # print(f"Setting {_setting} changed to {_state}")
    if _state == True:
        self.daq_gen_start_stop_values[_setting] = 0x01
    else:
        self.daq_gen_start_stop_values[_setting] = 0x00
    if self.daq_gen_start_stop_Qt_objects[_setting].isChecked() != _state:
        self.daq_gen_start_stop_Qt_objects[_setting].setChecked(_state)

def on_daq_gen_start_stop_changed_lineedit(self, _text, _setting):
    # print(f"Setting {_setting} changed to {_text}")
    long_number = int(_text)
    self.daq_gen_start_stop_values[_setting] = long_number
    if self.daq_gen_start_stop_Qt_objects[_setting].text() != _text:
        self.daq_gen_start_stop_Qt_objects[_setting].setText(_text)

def on_HV_asic_box_changed(self, _state, _fpga, _asic):
    # print(f"Setting HV for ASIC {_asic} changed to {_state}")
    if _state == True:
        self.hv_settings[_fpga * self.num_asic_tabs + _asic] = 0x01
    else:
        self.hv_settings[_fpga * self.num_asic_tabs + _asic] = 0x00

    if self.hv_checkboxes[_fpga * self.num_asic_tabs + _asic].isChecked() != _state:
        self.hv_checkboxes[_fpga * self.num_asic_tabs + _asic].setChecked(_state)

def on_HV_fpga_box_changed(self, _state, _fpga):
    # print(f"Setting HV for FPGA {_fpga} changed to {_state}")
    if _state == True:
        for _asic in range(self.num_asic_tabs):
            self.hv_settings[_fpga * self.num_asic_tabs + _asic] = 0x01
            if self.hv_checkboxes[_fpga * self.num_asic_tabs + _asic].isChecked() != _state:
                self.hv_checkboxes[_fpga * self.num_asic_tabs + _asic].setChecked(_state)
    else:
        for _asic in range(self.num_asic_tabs):
            self.hv_settings[_fpga * self.num_asic_tabs + _asic] = 0x00
            if self.hv_checkboxes[_fpga * self.num_asic_tabs + _asic].isChecked() != _state:
                self.hv_checkboxes[_fpga * self.num_asic_tabs + _asic].setChecked(_state)

def on_HV_send_button_pushed(self):
    # print("Sending HV settings")
    if self.stop_DAQ_flag == False:
        QMessageBox.critical(self, "HV Setting Stopped", "Data acquisition must be stopped before sending HV settings", QMessageBox.Ok)
        return

    self.worker.set_HV_settings(self)

# def on_auto_daq_gen_start_stop_changed(self, status):
#     self.auto_daq_gen_start_stop = status
#     if self.gen_start_stop_auto_select.isChecked() != status:
#         self.gen_start_stop_auto_select.setChecked(status)

def on_daq_gen_start_stop_send_button_pushed(self):
    _daq_push       = self.daq_gen_start_stop_values['daq_push']
    _gen_start_stop = self.daq_gen_start_stop_values['generator_start_stop']
    _daq_start_stop = self.daq_gen_start_stop_values['daq_start_stop']

    # if _daq_push != 0:
    #     if _gen_start_stop !=0:
    #         logger.critical("DAQ push and generator start/stop cannot be enabled at the same time")
    #         QMessageBox.warning(self, "DAQ/Generator Conflict", "DAQ push and generator start/stop cannot be enabled at the same time", QMessageBox.Ok)
    #         return
    # elif _daq_start_stop == 0 and _gen_start_stop == 0:
    #     logger.critical("At least one of the options must be enabled")
    #     QMessageBox.warning(self, "No Option Selected", "At least one of the options must be enabled", QMessageBox.Ok)
    #     return

    self.worker.send_daq_gen_start_stop(self)

def on_gen_settings_apply_button_pushed(self):
    self.worker.send_gen_settings(self)

def on_mute_button_pushed(self):
    if self.is_muted == False:
        self.mute_button.setIcon(QIcon(self.mute_icon_unmute))
        self.is_muted = True
    else:
        self.mute_button.setIcon(QIcon(self.mute_icon_mute))
        self.is_muted = False

def on_gen_settings_auto_select_checkbox_changed(self, status):
    # logger.debug(f"Auto select checkbox changed to {status}")
    if self.gen_settings_auto_select.isChecked() != status:
        self.gen_settings_auto_select.setChecked(status)

    self.send_gen_before_DAQ = status

def on_reset_cnt_auto_select_checkbox_changed(self, status):
    # logger.debug(f"Auto select checkbox changed to {status}")
    if self.reset_cnt_checkbox.isChecked() != status:
        self.reset_cnt_checkbox.setChecked(status)
    
    self.reset_cnt_before_DAQ = status

def on_run_monitoring_checkbox_changed(self, status):
    if self.run_monitoring_checkbox.isChecked() != status:
        self.run_monitoring_checkbox.setChecked(status)
    
    self.run_online_monitoring = status

def on_reset_adj_changed_spinbox(self, value, _key):
    self.reset_adj_values[_key] = value
    if self.reset_adj_Qt_objects[_key].value() != value:
        self.reset_adj_Qt_objects[_key].setValue(value)

def on_reset_adj_changed_checkbox(self, checked, _key):
    if checked == True:
        self.reset_adj_values[_key] = 0x01
    else:
        self.reset_adj_values[_key] = 0x00

    if self.reset_adj_Qt_objects[_key].isChecked() != checked:
        self.reset_adj_Qt_objects[_key].setChecked(checked)

def start_monitoring(self):
    logger.debug("Starting monitoring!")
    root_function_call = "RunMonitoring.c(" + str(self.current_RunNumber) + ")"
    monitoring_executable_config = ['root', '-q', '-b', '-x', '-l', root_function_call]
    monitoring_executable_environemnt = os.environ.copy()
    monitoring_executable_environemnt['DATA_DIRECTORY'] = self.selected_working_folder
    log_file = os.path.join(self.selected_working_folder, f"monitoring_{self.current_RunNumber}.log")
    
    try:
        with open(log_file, 'w') as log:
            process = subprocess.Popen(monitoring_executable_config, cwd=self.monitoring_executable, stdout=log, stderr=log, env=monitoring_executable_environemnt, preexec_fn=os.setsid)
        self.monitoring_process = process
        logger.debug(f"Monitoring process started with PID: {process.pid}")
    except Exception as e:
        logger.error(f"Failed to start monitoring process: {e}")
        self.monitoring_process = None

def stop_monitoring(self):
    if self.monitoring_process is not None:
        if self.monitoring_process.poll() is None:
            logger.debug("Stopping monitoring!")
            os.killpg(os.getpgid(self.monitoring_process.pid), signal.SIGKILL)
            os.system("killall root")
        else:
            logger.debug("Monitoring process was not running.")
        self.monitoring_process = None
    else:
        logger.debug("No monitoring process to stop.")
