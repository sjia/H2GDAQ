import socket
import time

client_ip = '127.0.0.1'
client_port = 12345

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((client_ip, client_port))
sock.settimeout(0.1)

target_ip = '127.0.0.1'
target_port = 10001

example_data_byte_array = bytearray()
# fill with 40 bytes of incrementing data
for i in range(40):
    example_data_byte_array.append(i)

while True:
    # receive data from client
    # sock.sendto(example_data_byte_array, (target_ip, target_port))
    # time.sleep(0.1)
    try:
        data, addr = sock.recvfrom(1024)
    except socket.timeout:
        continue
    if data[10] & 0x01 == 0x01:
        print("Received data from client")
        print(data.hex())
        counter_value = 0
        while True:
            counter_value += 1
            if counter_value > 100:
                counter_value = 0
            print(f"Sending counter value {counter_value}")
            sock.sendto(example_data_byte_array, (target_ip, target_port))
            time.sleep(0.1)
            data = bytearray()
            try:
                data, addr = sock.recvfrom(1024)
            except socket.timeout:
                pass
            if data == bytearray():
                continue
            elif data[10] & 0x01 == 0x00 and data[2] == 0x07:
                print("Received stop signal from client")
                break
            else:
                print(data.hex())
    else:
        # just print the content in hex
        print(data.hex())