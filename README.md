# H2GDAQ

![UCPH](https://img.shields.io/badge/København_Universitet-NBI-blue) ![CERN](https://img.shields.io/badge/CERN-ALICE-critical)


- [H2GDAQ](#h2gdaq)
  - [Pre-requisites](#pre-requisites)
  - [Network Optimization](#network-optimization)
      - [Linux](#linux)
      - [Temporary Changes:](#temporary-changes)
      - [Permanent Changes:](#permanent-changes)
  - [Usage](#usage)
      - [Running from source](#running-from-source)
      - [Working Folder](#working-folder)
      - [DAQ Settings](#daq-settings)
      - [Generator Config](#generator-config)
      - [DAQ + Generator START/STOP (new in v0.5)](#daq--generator-startstop-new-in-v05)
      - [Debug Info (new in v0.7)](#debug-info-new-in-v07)
      - [High Voltage](#high-voltage)
      - [Reset/Adj](#resetadj)
  - [Configuration Files](#configuration-files)
  - [License](#license)
  - [Citation](#citation)

## Pre-requisites

- [PySide6](https://pypi.org/project/PySide6/)
- [Numpy](https://numpy.org)
- [Matplotlib](https://matplotlib.org)

You can install the required packages by running the following command:

```bash
python3 -m pip install pyside6 numpy matplotlib
```

For python version, python 3.12 is tested. And a minimum version of python 3.7 is required.

## Network Optimization

#### Linux

It is highly recommended to optimize the network settings for the UDP communication. The following commands will set the network settings for the UDP communication:

#### Temporary Changes:

```bash
sudo sysctl -w net.core.rmem_max=8388608
sudo sysctl -w net.core.wmem_max=8388608
sudo sysctl -w net.core.rmem_default=8388608
sudo sysctl -w net.core.wmem_default=8388608

sudo sysctl -w net.core.netdev_max_backlog=250000
sudo sysctl -w net.ipv4.udp_mem='8388608 8388608 8388608'
sudo sysctl -w net.ipv4.udp_rmem_min=16384
sudo sysctl -w net.ipv4.udp_wmem_min=16384
```

#### Permanent Changes:

Edit `/etc/sysctl.conf` and add the following lines:

```bash
net.core.rmem_max=8388608
net.core.wmem_max=8388608
net.core.rmem_default=8388608
net.core.wmem_default=8388608

net.core.netdev_max_backlog=250000
net.ipv4.udp_mem=8388608 8388608 8388608
net.ipv4.udp_rmem_min=16384
net.ipv4.udp_wmem_min=16384
```

Then run the following command to apply the changes:

```bash
sudo sysctl -p
```


## Usage

#### Running from source

To run the application, execute the following command:
    
> **NOTE**
> 
> The application should be executed from the root folder of the project to load the configuration files correctly.

```bash
python3 H2GDAQ.py
```

And you should see the following window:

<img src="doc/init_window_pic.png" alt="H2GDAQ_Init" width="500"/>

After choosing the desired configuration, you'll see the main window (v0.7):

<img src="doc/main_window_pic.png" alt="H2GDAQ_Main" width="700"/>

For quick start, you can use the `example_ex_config.ini` file in the `config` folder. Move this file to the working folder and rename it to `config.ini`. This configuration is tested with the external trigger mode.

#### Working Folder

On the top left corner of the window, you can select the working folder. This folder will be used to store the data files collected during the acquisition.

The `Print to Terminal` button (new in v0.5) will change the output of the application to the terminal. This is useful for debugging purposes. Please note that in this mode, only low rate operation is advised.

#### DAQ Settings

On the top right corner of the window, you can control the acquisition process. There are three DAQ modes:

- **Event Mode** (new in v0.4): The acquisition will stop after the specified number of events.
  > **IMPORTANT**
  >
  > The event mode is assuming every UDP packet is full, which is not the case in low rate operation. Therefore, **during low trigger rate ( < 10 Hz )**, the actual data size might be smaller than the specified size.

- **Time Mode**: The acquisition will stop after the specified time.
- **Manual Mode**: The acquisition will stop only when the user clicks the stop button.

The first checkbox for the lower half of `DAQ Settings` is `Reset Pack Counter`. This will reset the packet counter. By enabling this, the packet counter will be reset every time the acquisition starts. (new in v0.9)

Below you can find run number setting spinbox. The typical data file name will be `run_XXXX.dat`, where `XXXX` is the run number.

Then here is a `Data Count` label, which shows the data count collected during the acquisition. Mind that this is including the UDP headers, so the actual data size will be smaller.

On the bottom there are three buttons:

- **Set IODELAY**: Set the IODELAY settings for ASICs. Note that this necessary every time you start the hardware. And **it will set the Top Registers and give a soft reset to the ASICs.**
- **Start DAQ**: Start the acquisition.
- **Stop DAQ**: Stop the acquisition.

#### Generator Config

In the middle part, you can find the generator configuration. You can set the generator, which is very important for the DAQ configuration. You don't need to send the settings manually, the application will send the settings when you start the DAQ.

#### DAQ + Generator START/STOP (new in v0.5)

Here you can find the FPGA DAQ and Generator start/stop controls. These buttons are only available when the DAQ is running.

- **DAQ Push**: To manually send `DAQ PUSH` command
- **Start Generator**: To manually send `START GENERATOR` command
- **Stop Generator**: To manually send `STOP GENERATOR` command

#### Debug Info (new in v0.7)

In the another tab, you can find the debug information. Just request the debug info for one ASIC and the application will show the debug information in a table.

#### High Voltage

On the bottom right corner, you can find the high voltage settings. **This part is independent of other settings**. And requires manual sending of the settings.

#### Reset/Adj

Here you can reset the H2GCROCs manually. **The I2C reset is not implemented in firmware yet**.
By using the `Adjustment` value, you can let the FPGA to search for the fixed bit sequence in the data. This is useful for the alignment in serial parallel conversion. 

## Configuration Files

In the `config` folder, you can find the configuration files for the different configurations. Serval configurations are provided as examples.

- `udp_config.json`: Configuration for the UDP communication. Including the IP addresses and ports for the different KCUs and the PC.
- `h2gcroc_1v4_r1.json`: Configuration for the H2GCROC registers. **Users should not modify this file**.
- `example_ex_config.ini`: Example configuration for the external injection. Move this file to the working folder and rename it to `config.ini` to use it.

After the first run, a `config.ini` file will be created in the same folder as the application. This file stores the last configuration used by the user. Deleting this file will reset the configuration to the default one.

## License

This project is licensed under the Creative Commons Attribution 4.0 International License (CC BY 4.0). See the [LICENSE](LICENSE) file for details.

## Citation

If you use this software for research purposes, please cite it as follows: [CITATION](CITATION)