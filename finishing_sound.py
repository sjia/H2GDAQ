import pygame

def play_finishing_sound():
    # pass
    pygame.mixer.init()
    pygame.mixer.music.load("asset/finish_converted.wav")
    pygame.mixer.music.set_volume(1.0)
    pygame.mixer.music.play()