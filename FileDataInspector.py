import sys
from loguru import logger

log_format = "<level>{time:YYYY-MM-DD HH:mm:ss} | {level:.1s} | {message}</level>"
logger.remove()  # Remove default handler
logger.add(sink=sys.stderr, format=log_format)

file_path = "dump/Run004.h2g"
logger.info(f"Inspecting file: {file_path}")

content_buffer = bytearray()
last_counter = 0
is_first_packet = True

total_packets = 0
nonconsecutive_packets = 0

# Read the binary file
with open(file_path, "rb") as f:
    # Read in larger chunks
    chunk_size = 4096
    while True:
        chunk = f.read(chunk_size)
        if not chunk:
            break
        
        content_buffer.extend(chunk)
        
        while len(content_buffer) >= 1452:
            if content_buffer[0] == 35 and is_first_packet:
                # translate comment line to string and print
                print(content_buffer[:content_buffer.find(b'\n') + 1].decode(), end="")
                content_buffer = content_buffer[content_buffer.find(b'\n') + 1:]
                continue

            packet_counter = content_buffer[3] + (content_buffer[2] << 8)
            if packet_counter != last_counter + 1:
                if packet_counter != 0:
                    if is_first_packet:
                        is_first_packet = False
                    else:
                        nonconsecutive_packets += 1
                        # print(f"Non-consecutive packet: last={last_counter}, current={packet_counter}")
            last_counter = packet_counter

            # Remove the first 1452 bytes from the buffer
            del content_buffer[:1452]
            total_packets += 1

logger.info(f"Total packets: {total_packets}")
logger.info(f"Non-consecutive packets: {nonconsecutive_packets}")
logger.info(f"Lost packets rate: {nonconsecutive_packets / total_packets * 100:.6f}%")
