import sys, socket, time
from multiprocessing import Process, Queue, Value
from PySide6.QtCore import QThread, Signal, QObject, QTimer
from PySide6.QtWidgets import QApplication, QMainWindow, QPushButton, QMessageBox
import packetlib, queue
from loguru import logger
import H2GDAQ_components

log_format = "<level>{time:YYYY-MM-DD HH:mm:ss} | {level:.1s} | {message}</level>"
logger.remove()  # Remove default handler
logger.add(sink=sys.stderr, format=log_format)

def udp_receiver(ip, port, data_queue, output_queue, stop_flag):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 4096*16)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 4 * 1024 * 1024)
    sock.bind((ip, port))
    sock.setblocking(False)
    while not stop_flag.value:
        try:
            data, rev_addr = sock.recvfrom(115200)
            try:
                data_queue.put_nowait(data)
            except queue.Full:
                pass
        except BlockingIOError:
            time.sleep(0.001)
        except KeyboardInterrupt:
            break
        except Exception as e:
            logger.error(f"Error in udp_receiver: {e}")
            break
        try:
            if not output_queue.empty():
                _ip, _port, data_array = output_queue.get()
                sock.sendto(data_array, (_ip, _port))
                byte_array_str = ' '.join([f"{_byte:02X}" for _byte in data_array])
                logger.info(f"Sent to {_ip}:{_port}: {byte_array_str}")
        except KeyboardInterrupt:
            break
        except Exception as e:
            logger.error(f"Error in udp_receiver: {e}")
            break
    sock.close()

# def file_writer(queue, file_path, data_cnt, stop_flag):
#     with open(file_path, 'ab', buffering=4 * 1024 * 1024) as file:
#         local_data_cnt = 0
#         with data_cnt.get_lock():
#             data_cnt.value = 0

#         while not stop_flag.value:
#             data_list = []
#             try:
#                 # Attempt to get data from the queue with a timeout
#                 while len(data_list) < 500:  # Process a larger batch of 500 packets at a time
#                     try:
#                         data = queue.get(timeout=0.1)
#                         if data is None:
#                             continue
#                         data_list.append(data)
#                         local_data_cnt += len(data)
#                     except Empty:
#                         break

#                 if data_list:
#                     file.write(b''.join(data_list))
#                     # Flush the file every 5000 packets to reduce I/O overhead
#                     if len(data_list) >= 5000:
#                         file.flush()
#                         with data_cnt.get_lock():
#                             data_cnt.value += local_data_cnt
#                         local_data_cnt = 0

#                 if not data_list:
#                     # If no data was processed, sleep briefly to reduce CPU usage
#                     time.sleep(0.01)

#             except Exception as e:
#                 logger.error(f"Error in file_writer: {e}")
#                 break

#         # Final flush before exiting
#         file.flush()
#         with data_cnt.get_lock():
#             data_cnt.value += local_data_cnt

def file_writer(queue, file_path, data_cnt, stop_flag):
    with open(file_path, 'ab', buffering=4 * 1024 * 1024) as file:
        local_data_cnt = 0
        with data_cnt.get_lock():
            data_cnt.value = 0

        while not stop_flag.value:
            try:
                data_list = []
                while len(data_list) < 100:  # Batch process 100 packets at a time
                    try:
                        data = queue.get(timeout=0.1)
                        if data is None:
                            continue
                        data_list.append(data)
                        if data[0] != 0x23 and data[11] != 0x23:
                            local_data_cnt += len(data)
                    except Exception as e:
                        # logger.warning(f"File writer exited: {e}")
                        break

                if data_list:
                    file.write(b''.join(data_list))
                    file.flush()
                    with data_cnt.get_lock():
                        data_cnt.value += local_data_cnt
                    local_data_cnt = 0

            except Exception as e:
                logger.error(f"Error in file_writer: {e}")
                break

# def file_writer(queue, file_path, data_cnt, stop_flag):
#     with open(file_path, 'ab', buffering=4 * 1024 * 1024) as file:  # Open the file in binary append mode
#         local_data_cnt = 0
#         with data_cnt.get_lock():
#             data_cnt.value = 0
#         while not stop_flag.value:
#             try:
#                 data = queue.get(timeout=1)  # Timeout to periodically check stop_flag
#                 if data is None:
#                     continue
#                 local_data_cnt += len(data)
#                 file.write(data)
#                 file.flush()
#                 with data_cnt.get_lock():
#                     data_cnt.value += local_data_cnt
#                 local_data_cnt = 0
#             except Exception as e:
#                 logger.error(f"Error in file_writer: {e}")
#                 break
        # loop_cnt = 0
        # local_data_cnt = 0
        # with data_cnt.get_lock():
        #     data_cnt.value = 0
        # while not stop_flag.value:
        #     loop_cnt += 1
        #     data = queue.get()
        #     if data is None:  # Check for sentinel value to stop the process
        #         continue
        #     local_data_cnt += len(data)
        #     if loop_cnt >= 1:
        #         with data_cnt.get_lock():
        #             data_cnt.value += local_data_cnt
        #         local_data_cnt = 0
        #         loop_cnt = 0
        #     file.write(data)  
        #     file.flush()  # Optionally flush data immediately

def file_printer(queue, data_cnt, stop_flag):
    values_list_byte0 = [0xA0, 0xA1, 0xA2, 0xA3, 0xA4, 0xA5, 0xA6, 0xA7]
    values_list_byte1 = [0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]
    values_list_byte2 = [0x24, 0x25]
    values_list_byte3 = [0x00, 0x01, 0x02, 0x03, 0x04]
    color_list_byte0 = ['\033[31m', '\033[32m', '\033[33m', '\033[34m', '\033[35m', '\033[36m', '\033[37m', '\033[38m']
    color_list_byte1 = ['\033[91m', '\033[92m', '\033[93m', '\033[94m', '\033[95m', '\033[96m', '\033[97m', '\033[98m']
    color_list_byte2 = ['\033[32m', '\033[34m']
    color_list_byte3 = ['\033[33m', '\033[35m', '\033[36m', '\033[37m', '\033[38m']
    color_timestamp = '\033[37m'
    color_reset = '\033[0m'
    loop_cnt = 0
    local_data_cnt = 0
    with data_cnt.get_lock():
        data_cnt.value = 0
    while not stop_flag.value:
        loop_cnt += 1
        data = queue.get()
        if data is None:
            break
        if data[0] != 0x23 and data[11] != 0x23:
            local_data_cnt += len(data)
        if loop_cnt >= 1:
            with data_cnt.get_lock():
                data_cnt.value += local_data_cnt
            local_data_cnt = 0
            loop_cnt = 0
        # print first 12 bytes
        print(f'--- packet header:', end=' ')
        for _index in range(12):
            print(f"{data[_index]:02X}", end=' ')
        print()
        for _index in range(12, len(data)):
            _byte0 = data[_index]
            try:
                _byte1 = data[_index + 1]
                _byte2 = data[_index + 2]
                _byte3 = data[_index + 3]
            except:
                break
            if _byte0 in values_list_byte0:
                if _byte1 in values_list_byte1:
                    if _byte2 in values_list_byte2:
                        if _byte3 in values_list_byte3:
                            try:
                                # print the header
                                print(f"{color_list_byte0[values_list_byte0.index(_byte0)]}{_byte0:02X}{color_reset} ", end='')
                                print(f"{color_list_byte1[values_list_byte1.index(_byte1)]}{_byte1:02X}{color_reset} ", end='')
                                print(f"{color_list_byte2[values_list_byte2.index(_byte2)]}{_byte2:02X}{color_reset} ", end='')
                                print(f"{color_list_byte3[values_list_byte3.index(_byte3)]}{_byte3:02X}{color_reset} ", end='')
                                for _i in range(4, 7):
                                    print(f"{color_timestamp}{data[_index + _i]:02X}{color_reset} ", end='')
                                print(f"{color_timestamp}{data[_index + 7]:02X}{color_reset}", end='|')
                                for _word in range(8, 40, 4):
                                    for _i in range(3):
                                        print(f"{data[_index + _word + _i]:02X}", end=' ')
                                    print(f"{data[_index + _word + 3]:02X}", end='|')
                                print()
                                _index += 40
                            except:
                                break
        print(f'--- packet end ---')

class UdpSocketWorker(QObject):
    timer_update_signal = Signal(int)
    event_update_signal = Signal(int)
    daq_timeout_signal = Signal()
    heartbeat_signal = Signal()
    daq_event_timeout_signal = Signal()
    def __init__(self, parent, _ip, _port):
        super().__init__(parent)
        self.ip = _ip
        self.port = _port
        self.data_queue = Queue()
        self.output_queue = Queue()
        self.data_cnt = Value('d', 0) # double float
        self.timer = QTimer()
        self.heartbeat_timer = QTimer()
        self.progress_timer = QTimer()
        self.progress_timer.timeout.connect(self.update_progress)
        self.event_timer = QTimer()
        self.event_timer.timeout.connect(self.update_event)

        self.data_cnt_update_timer = QTimer(self)
        self.data_cnt_update_timer.timeout.connect(self.update_data_cnt)
        self.data_cnt_update_timer.start(1000)

        self.receiver_process   = Process()
        self.writer_process     = Process()
        self.printer_process    = Process()
        self.file_path = 'dump/received_data.txt'

        self.udp_stop_flag = Value('b', 0)

        self.mainwindow = None

        self.target_data_bytes = 0

    def send_reset_pack_counter(self, mainwindow):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            for _fpga in range(mainwindow.num_fpga_tabs):
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                try:
                    sock.bind((self.ip, self.port))
                except Exception as e:
                    QMessageBox.warning(mainwindow, "Error", f"Error binding to {self.ip}:{self.port}\n{e}")
                    sock.close()
                    return

                _target_ip = mainwindow.link_ip_address_list[_fpga]
                _target_port = int(mainwindow.link_port_list[_fpga])
                _reset_asic_bits = (0x01 << mainwindow.num_asic_tabs) - 1

                if packetlib.send_reset_adj(sock, _target_ip, _target_port, 0x00, _fpga, sw_hard_reset_sel=0x00, sw_hard_reset=0x00, sw_soft_reset_sel=0x00, sw_soft_reset=0x00, sw_i2c_reset_sel=0x00, sw_i2c_reset=0x00, reset_pack_counter=_reset_asic_bits, adjustable_start=0x00, verbose=True) != True:
                    QMessageBox.error(mainwindow, "Error", f"Error sending reset adj settings to {_target_ip}:{_target_port}")
                    return
        sock.close()

    def start_datataking_process(self, mainwindow, file_path):
        self.mainwindow = mainwindow
        self.file_path = file_path
        self.udp_stop_flag.value = 0
        if mainwindow.send_gen_before_DAQ:
            # ! === send generator settings to FPGA ===
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                try:
                    sock.bind((self.ip, self.port))
                except Exception as e:
                    H2GDAQ_components.on_stop_DAQ(mainwindow)
                    QMessageBox.warning(mainwindow, "Error", f"Error binding to {self.ip}:{self.port}\n{e}")
                    sock.close()
                    return

                for _fpga in range(mainwindow.num_fpga_tabs):
                    _target_ip = mainwindow.link_ip_address_list[_fpga]
                    _target_port = int(mainwindow.link_port_list[_fpga])
                    if packetlib.send_check_DAQ_gen_params(
                        sock, _target_ip, _target_port, 0x00, _fpga, 
                        data_coll_en        = mainwindow.gen_setting_values['data_coll_enable'], 
                        trig_coll_en        = mainwindow.gen_setting_values['trig_coll_enable'], 
                        daq_fcmd            = mainwindow.gen_setting_values['daq_fcmd'],
                        gen_pre_fcmd        = mainwindow.gen_setting_values['generator_pre_fcmd'],
                        gen_fcmd            = mainwindow.gen_setting_values['generator_fcmd'],
                        ext_trg_en          = mainwindow.gen_setting_values['ext_trig_enable'],
                        ext_trg_delay       = mainwindow.gen_setting_values['ext_trig_delay'],
                        ext_trg_deadtime    = mainwindow.gen_setting_values['ext_trig_deadtime'],
                        gen_preimp_en       = mainwindow.gen_setting_values['generator_preimp_enable'],
                        gen_pre_interval    = mainwindow.gen_setting_values['generator_pre_interval'],
                        gen_nr_of_cycle     = mainwindow.gen_setting_values['generator_nr_of_cycles'],
                        gen_interval        = mainwindow.gen_setting_values['generator_interval'],
                        daq_push_fcmd       = mainwindow.gen_setting_values['daq_push_fcmd'],
                        machine_gun         = mainwindow.gen_setting_values['machine_gun'],
                        ext_trg_out_0_len   = 0x00, 
                        ext_trg_out_1_len   = 0x00, 
                        ext_trg_out_2_len   = 0x00, 
                        ext_trg_out_3_len   = 0x00, 
                        verbose=False, readback=True):
                        logger.info(f"Gen settings sent to {_target_ip}:{_target_port}")
                    
            sock.close()
            # make sure the address is released
            time.sleep(0.5)
        self.heartbeat_timer = QTimer()
        self.heartbeat_timer.timeout.connect(self.on_heartbeat_timeout)
        self.heartbeat_timer.start(1000)
        self.send_daq_gen_start_stop_start(mainwindow)
        # ! === finish generator settings ===
        if not self.receiver_process.is_alive():
            self.receiver_process = Process(target=udp_receiver, args=(self.ip, self.port, self.data_queue, self.output_queue, self.udp_stop_flag))
            self.receiver_process.start()
        
        if not self.writer_process.is_alive():
            self.writer_process = Process(target=file_writer, args=(self.data_queue, self.file_path, self.data_cnt, self.udp_stop_flag))
            self.writer_process.start()

    def start_datataking_process_terminal(self, mainwindow):
        self.mainwindow = mainwindow
        self.udp_stop_flag.value = 0
        if mainwindow.send_gen_before_DAQ:
            # ! === send generator settings to FPGA ===
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
                sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                sock.bind((self.ip, self.port))

                for _fpga in range(mainwindow.num_fpga_tabs):
                    _target_ip = mainwindow.link_ip_address_list[_fpga]
                    _target_port = int(mainwindow.link_port_list[_fpga])
                    if packetlib.send_check_DAQ_gen_params(
                        sock, _target_ip, _target_port, 0x00, _fpga, 
                        data_coll_en        = mainwindow.gen_setting_values['data_coll_enable'], 
                        trig_coll_en        = mainwindow.gen_setting_values['trig_coll_enable'], 
                        daq_fcmd            = mainwindow.gen_setting_values['daq_fcmd'],
                        gen_pre_fcmd        = mainwindow.gen_setting_values['generator_pre_fcmd'],
                        gen_fcmd            = mainwindow.gen_setting_values['generator_fcmd'],
                        ext_trg_en          = mainwindow.gen_setting_values['ext_trig_enable'],
                        ext_trg_delay       = mainwindow.gen_setting_values['ext_trig_delay'],
                        ext_trg_deadtime    = mainwindow.gen_setting_values['ext_trig_deadtime'],
                        gen_preimp_en       = mainwindow.gen_setting_values['generator_preimp_enable'],
                        gen_pre_interval    = mainwindow.gen_setting_values['generator_pre_interval'],
                        gen_nr_of_cycle     = mainwindow.gen_setting_values['generator_nr_of_cycles'],
                        gen_interval        = mainwindow.gen_setting_values['generator_interval'],
                        daq_push_fcmd       = mainwindow.gen_setting_values['daq_push_fcmd'],
                        machine_gun         = mainwindow.gen_setting_values['machine_gun'],
                        ext_trg_out_0_len   = 0x00, 
                        ext_trg_out_1_len   = 0x00, 
                        ext_trg_out_2_len   = 0x00, 
                        ext_trg_out_3_len   = 0x00, 
                        verbose=True, readback=True):
                        logger.info(f"Gen settings sent to {_target_ip}:{_target_port}")
                    
            sock.close()
            
            # make sure the address is released
            time.sleep(0.5)
            self.heartbeat_timer = QTimer()
            self.heartbeat_timer.timeout.connect(self.on_heartbeat_timeout)
            self.heartbeat_timer.start(1000)
        # if mainwindow.auto_daq_gen_start_stop:
        self.send_daq_gen_start_stop_start(mainwindow)
        # ! === finish generator settings ===
        if not self.receiver_process.is_alive():
            self.receiver_process = Process(target=udp_receiver, args=(self.ip, self.port, self.data_queue, self.output_queue, self.udp_stop_flag))
            self.receiver_process.start()
        
        if not self.printer_process.is_alive():
            self.printer_process = Process(target=file_printer, args=(self.data_queue, self.data_cnt, self.udp_stop_flag))
            self.printer_process.start()

    def send_gen_settings(self, mainwindow):
        self.mainwindow = mainwindow

        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            try:
                sock.bind((self.ip, self.port))
            except Exception as e:
                QMessageBox.warning(mainwindow, "Error", f"Error binding to {self.ip}:{self.port}\n{e}")
                sock.close()
                return

            for _fpga in range(mainwindow.num_fpga_tabs):
                _target_ip = mainwindow.link_ip_address_list[_fpga]
                _target_port = int(mainwindow.link_port_list[_fpga])
                if packetlib.send_check_DAQ_gen_params(
                    sock, _target_ip, _target_port, 0x00, _fpga, 
                    data_coll_en        = mainwindow.gen_setting_values['data_coll_enable'], 
                    trig_coll_en        = mainwindow.gen_setting_values['trig_coll_enable'], 
                    daq_fcmd            = mainwindow.gen_setting_values['daq_fcmd'],
                    gen_pre_fcmd        = mainwindow.gen_setting_values['generator_pre_fcmd'],
                    gen_fcmd            = mainwindow.gen_setting_values['generator_fcmd'],
                    ext_trg_en          = mainwindow.gen_setting_values['ext_trig_enable'],
                    ext_trg_delay       = mainwindow.gen_setting_values['ext_trig_delay'],
                    ext_trg_deadtime    = mainwindow.gen_setting_values['ext_trig_deadtime'],
                    gen_preimp_en       = mainwindow.gen_setting_values['generator_preimp_enable'],
                    gen_pre_interval    = mainwindow.gen_setting_values['generator_pre_interval'],
                    gen_nr_of_cycle     = mainwindow.gen_setting_values['generator_nr_of_cycles'],
                    gen_interval        = mainwindow.gen_setting_values['generator_interval'],
                    daq_push_fcmd       = mainwindow.gen_setting_values['daq_push_fcmd'],
                    machine_gun         = mainwindow.gen_setting_values['machine_gun'],
                    ext_trg_out_0_len   = 0x00, 
                    ext_trg_out_1_len   = 0x00, 
                    ext_trg_out_2_len   = 0x00, 
                    ext_trg_out_3_len   = 0x00, 
                    verbose=True, readback=True):
                    logger.info(f"Gen settings sent to {_target_ip}:{_target_port}")
                
        sock.close()
        time.sleep(0.5)

    def create_heartbeat_packet(self, mainwindow):
        current_time_sec = int(time.time())
        current_time_msec = int((time.time() - current_time_sec) * 1000)
        # create a 1452 bytes packet
        _heartbeat_packet = []
        _heartbeat_packet.extend([0x23] * 12)
        # followed by timestamp
        _heartbeat_packet.extend([current_time_sec & 0xFF, (current_time_sec >> 8) & 0xFF, (current_time_sec >> 16) & 0xFF, (current_time_sec >> 24) & 0xFF])
        _heartbeat_packet.extend([current_time_msec & 0xFF, (current_time_msec >> 8) & 0xFF, (current_time_msec >> 16) & 0xFF, (current_time_msec >> 24) & 0xFF])
        # followed by 1432 bytes of 0x36
        _heartbeat_packet.extend([0x36] * 1432)
        # translate to bytes
        _heartbeat_packet = bytes(_heartbeat_packet)

        return _heartbeat_packet

    def get_debug_data(self, mainwindow, _fpga, _asic):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.bind((self.ip, self.port))
            _target_ip = mainwindow.link_ip_address_list[_fpga]
            _target_port = int(mainwindow.link_port_list[_fpga])

            debug_data = packetlib.get_debug_data(sock, _target_ip, _target_port, _asic, _fpga, verbose=True)
        
        sock.close()
        return debug_data

    def start_datataking_process_timed(self, mainwindow, file_path, _sec):
        self.sec_setting = _sec
        self.start_datataking_process(mainwindow, file_path)
        self.timer = QTimer()
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.on_timer_timeout)
        self.timer.start(_sec * 1000)


        self.progress_timer.start(_sec * 10)

    def start_datataking_process_timed_terminal(self, mainwindow, _sec):
        self.sec_setting = _sec
        self.start_datataking_process_terminal(mainwindow)
        self.timer = QTimer()
        self.timer.setSingleShot(True)
        self.timer.timeout.connect(self.on_timer_timeout)
        self.timer.start(_sec * 1000)

        self.progress_timer.start(_sec * 10)

    def start_datataking_process_event(self, mainwindow, file_path, _event):
        self.target_data_bytes = 400 # 40 bytes per line, 5 lines per half and 2 halves per ASIC
        self.target_data_bytes = self.target_data_bytes * mainwindow.num_fpga_tabs * mainwindow.num_asic_tabs
        if mainwindow.gen_setting_values['generator_nr_of_cycles'] > 1:
            self.target_data_bytes = self.target_data_bytes * mainwindow.gen_setting_values['generator_nr_of_cycles']
        self.target_data_bytes = self.target_data_bytes * (mainwindow.gen_setting_values['machine_gun'] + 1)
        self.target_data_bytes = self.target_data_bytes * _event
        target_packet_num = (self.target_data_bytes // 1440) + 1
        self.target_data_bytes = target_packet_num * 1452
        logger.info(f"Target data bytes: {self.target_data_bytes}")
        self.start_datataking_process(mainwindow, file_path)
        self.event_timer.start(100)

    def start_datataking_process_event_terminal(self, mainwindow, _event):
        self.target_data_bytes = 400 # 40 bytes per line, 5 lines per half and 2 halves per ASIC
        self.target_data_bytes = self.target_data_bytes * mainwindow.num_fpga_tabs * mainwindow.num_asic_tabs
        # if mainwindow.gen_setting_values['generator_nr_of_cycles'] > 1:
        #     self.target_data_bytes = self.target_data_bytes * mainwindow.gen_setting_values['generator_nr_of_cycles']
        self.target_data_bytes = self.target_data_bytes * (mainwindow.gen_setting_values['machine_gun'] + 1)
        self.target_data_bytes = self.target_data_bytes * _event
        target_packet_num = (self.target_data_bytes // 1440) + 1
        self.target_data_bytes = target_packet_num * 1452
        logger.info(f"Target data bytes: {self.target_data_bytes}")
        self.start_datataking_process_terminal(mainwindow)
        self.event_timer.start(100)
        
    def on_timer_timeout(self):
        self.daq_timeout_signal.emit()

    def on_heartbeat_timeout(self):
        _heartbeat_packet = self.create_heartbeat_packet(self.mainwindow)
        self.data_queue.put(_heartbeat_packet)

    def get_data_cnt(self):
        with self.data_cnt.get_lock():
            return self.data_cnt.value
        
    def update_data_cnt(self):
        if self.mainwindow is not None:
            data_cnt = self.get_data_cnt()
            if data_cnt < 1e3:
                self.mainwindow.data_cnt_label.setText(f"{data_cnt} bytes")
            elif data_cnt < 1e6:
                data_cnt = data_cnt / 1024
                self.mainwindow.data_cnt_label.setText(f"{data_cnt:.2f} KB")
            elif data_cnt < 1e9:
                data_cnt = data_cnt / 1024 / 1024
                self.mainwindow.data_cnt_label.setText(f"{data_cnt:.2f} MB")
            else:
                data_cnt = data_cnt / 1024 / 1024 / 1024
                self.mainwindow.data_cnt_label.setText(f"{data_cnt:.2f} GB")

    def stop_datataking_process(self, mainwindow):
        self.send_daq_gen_start_stop_stop(mainwindow)
        time.sleep(0.5)

        with self.udp_stop_flag.get_lock():
            self.udp_stop_flag.value = 1
        if self.receiver_process.is_alive():
            self.receiver_process.terminate()
            self.receiver_process.join()
        if self.writer_process.is_alive():
            self.data_queue.put(None)
            self.writer_process.join()
        if self.printer_process.is_alive():
            self.data_queue.put(None)
            self.printer_process.join()
        if self.timer.isActive():
            self.timer.stop()
        if self.heartbeat_timer.isActive():
            self.heartbeat_timer.stop()
        if self.progress_timer.isActive():
            self.progress_timer.stop()
        if self.event_timer.isActive():
            self.event_timer.stop()
        time.sleep(0.5)

        # if mainwindow.auto_daq_gen_start_stop:
        
        # ! === finish generator settings ===

    def update_progress(self):
        time_passed = self.sec_setting - self.timer.remainingTime() / 1000
        precentage = time_passed / self.sec_setting * 100
        self.timer_update_signal.emit(precentage)

        self.progress_timer.start(self.sec_setting * 10)

    def update_event(self):
        data_cnt = self.get_data_cnt()
        precentage = data_cnt / self.target_data_bytes * 100
        self.event_update_signal.emit(precentage)

        if data_cnt >= self.target_data_bytes:
            self.mainwindow.DAQ_completed_flag = True
            self.daq_event_timeout_signal.emit()
            self.event_timer.stop()

    def set_HV_settings(self, mainwindow):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            try:
                sock.bind((self.ip, self.port))
            except Exception as e:
                QMessageBox.warning(mainwindow, "Error", f"Error binding to {self.ip}:{self.port}\n{e}")
                sock.close()
                return

            for _fpga in range(mainwindow.num_fpga_tabs):
                _control_byte = 0x00
                for _asic in range(mainwindow.num_asic_tabs):
                    hv_on = mainwindow.hv_settings[_fpga * mainwindow.num_asic_tabs + _asic]
                    _control_byte |= hv_on << _asic
                _content_40byte = []
                _content_40byte.append(0xA0)
                _content_40byte.append(_fpga)
                _content_40byte.append(0x03)
                _content_40byte.append(_control_byte)
                while len(_content_40byte) < 40:
                    _content_40byte.append(0x00)

                _target_ip = mainwindow.link_ip_address_list[_fpga]
                _target_port = int(mainwindow.link_port_list[_fpga])

                try:
                    sock.sendto(bytes(_content_40byte), (_target_ip, _target_port))
                except Exception as e:
                    QMessageBox.warning(mainwindow, "Error", f"Error sending HV settings to {_target_ip}:{_target_port}\n{e}")
                    continue
                logger.info(f"HV settings sent to {_target_ip}:{_target_port}")

            sock.close()

    def send_reset_adj_settings(self, mainwindow, _fpga):
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            try:
                sock.bind((self.ip, self.port))
            except Exception as e:
                QMessageBox.warning(mainwindow, "Error", f"Error binding to {self.ip}:{self.port}\n{e}")
                sock.close()
                return

            _target_ip = mainwindow.link_ip_address_list[_fpga]
            _target_port = int(mainwindow.link_port_list[_fpga])

            if packetlib.send_reset_adj(sock, _target_ip, _target_port, 0x00, _fpga, sw_hard_reset_sel=mainwindow.reset_adj_values['SW_HARD_RESET_SELECT'], sw_hard_reset=mainwindow.reset_adj_values['SW_HARD_RESET'], sw_soft_reset_sel=mainwindow.reset_adj_values['SW_SOFT_RESET_SELECT'], sw_soft_reset=mainwindow.reset_adj_values['SW_SOFT_RESET'], sw_i2c_reset_sel=mainwindow.reset_adj_values['SW_I2C_RESET_SELECT'], sw_i2c_reset=mainwindow.reset_adj_values['SW_I2C_RESET'], reset_pack_counter=mainwindow.reset_adj_values['RESET_PACK_COUNTER'], adjustable_start=mainwindow.reset_adj_values['ADJUSTABLE_START'], verbose=True) != True:
                QMessageBox.error(mainwindow, "Error", f"Error sending reset adj settings to {_target_ip}:{_target_port}")
                return
            
        sock.close()

    def send_daq_push(self, mainwindow):
        for _fpga in range(mainwindow.num_fpga_tabs):
            _fpga_ip = mainwindow.link_ip_address_list[_fpga]
            _fpga_port = int(mainwindow.link_port_list[_fpga])
            _data_array = packetlib.pack_data_req_daq_gen_start(0xA0, _fpga, daq_push=0xFF, gen_start_stop=0x00, daq_start_stop=0xFF)
            self.output_queue.put([_fpga_ip, _fpga_port, _data_array])

    def send_gen_start(self, mainwindow):
        for _fpga in range(mainwindow.num_fpga_tabs):
            _fpga_ip = mainwindow.link_ip_address_list[_fpga]
            _fpga_port = int(mainwindow.link_port_list[_fpga])
            _data_array = packetlib.pack_data_req_daq_gen_start(0xA0, _fpga, daq_push=0x00, gen_start_stop=0x01, daq_start_stop=0xFF)
            self.output_queue.put([_fpga_ip, _fpga_port, _data_array])

    def send_gen_stop(self, mainwindow):
        for _fpga in range(mainwindow.num_fpga_tabs):
            _fpga_ip = mainwindow.link_ip_address_list[_fpga]
            _fpga_port = int(mainwindow.link_port_list[_fpga])
            _data_array = packetlib.pack_data_req_daq_gen_start(0xA0, _fpga, daq_push=0x00, gen_start_stop=0x00, daq_start_stop=0xFF)
            self.output_queue.put([_fpga_ip, _fpga_port, _data_array])

    def send_daq_gen_start_stop_start(self, mainwindow):
            _daq_push       = 0x00
            _gen_start_stop = 0x00
            _daq_start_stop = 0xFF
            
            for _fpga in range(mainwindow.num_fpga_tabs):
                _fpga_ip = mainwindow.link_ip_address_list[_fpga]
                _fpga_port = int(mainwindow.link_port_list[_fpga])
                _data_array = packetlib.pack_data_req_daq_gen_start(0xA0, _fpga, daq_push=_daq_push, gen_start_stop=_gen_start_stop, daq_start_stop=_daq_start_stop)
                self.output_queue.put([_fpga_ip, _fpga_port, _data_array])

    def send_daq_gen_start_stop_stop(self, mainwindow):
        _daq_push       = 0x00
        _gen_start_stop = 0x00
        _daq_start_stop = 0x00

        for _fpga in range(mainwindow.num_fpga_tabs):
            _fpga_ip = mainwindow.link_ip_address_list[_fpga]
            _fpga_port = int(mainwindow.link_port_list[_fpga])
            _data_array = packetlib.pack_data_req_daq_gen_start(0xA0, _fpga, daq_push=_daq_push, gen_start_stop=_gen_start_stop, daq_start_stop=_daq_start_stop)
            self.output_queue.put([_fpga_ip, _fpga_port, _data_array])

    def send_phase_settings(self, mainwindow, _phase):
        if _phase < 0 or _phase > 15:
            logger.error(f"Invalid phase setting: {_phase}")
            return
        _top_reg_content = mainwindow.i2c_top_reg
        _top_reg_content[7] = _phase & 0x0F

        for _fpga in range(mainwindow.num_fpga_tabs):
            _fpga_ip = mainwindow.link_ip_address_list[_fpga]
            _fpga_port = int(mainwindow.link_port_list[_fpga])
            for _asic in range(2):
                _fpga_header = 0xA0 + _asic
                _data_len = len(_top_reg_content)
                _sub_addr = packetlib.subblock_address_dict["Top"]
                _subaddr_10_3 = (_sub_addr >> 3) & 0xFF
                _subaddr_2_0 = _sub_addr & 0x07
                _data_array = packetlib.pack_data_req_i2c_write(_fpga_header, _fpga, 0x00, _data_len, _subaddr_10_3, _subaddr_2_0, 0x00, _top_reg_content + [0x00] * (32 - _data_len))
                self.output_queue.put([_fpga_ip, _fpga_port, _data_array])

    # def send_daq_gen_start_stop_stop2(self, mainwindow):
    #     with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
    #         sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    #         sock.bind((self.ip, self.port))
    #     # if True:

    #         _daq_push       = mainwindow.daq_gen_start_stop_values['daq_push']
    #         _gen_start_stop = mainwindow.daq_gen_start_stop_values['generator_start_stop']
    #         _daq_start_stop = mainwindow.daq_gen_start_stop_values['daq_start_stop']

    #         if _daq_push != 0:
    #             if _gen_start_stop !=0:
    #                 logger.critical("DAQ push and generator start/stop cannot be enabled at the same time")
    #                 return
    #             else:
    #                 for _fpga in range(mainwindow.num_fpga_tabs):
    #                     for _asic in range(mainwindow.num_asic_tabs):
    #                         _target_ip = mainwindow.link_ip_address_list[_fpga]
    #                         _target_port = int(mainwindow.link_port_list[_fpga])
    #                         if packetlib.send_daq_gen_start_stop(sock, _target_ip, _target_port, _asic, _fpga, daq_push=_daq_push, gen_start_stop=0x00, daq_start_stop=_daq_start_stop, verbose=False):
    #                             logger.info(f"Gen start/stop sent to {_target_ip}:{_target_port}")
    #         for _fpga in range(mainwindow.num_fpga_tabs):
    #             # _data_array = packetlib.pack_data_req_daq_gen_start(0xA0, _fpga, daq_push=0x00, gen_start_stop=0x00, daq_start_stop=0x00)
    #             # # with self.output_queue.lock:
    #             # self.output_queue.put(_data_array)
    #             _asic = 0x00
    #             _target_ip = mainwindow.link_ip_address_list[_fpga]
    #             _target_port = int(mainwindow.link_port_list[_fpga])
    #             if packetlib.send_daq_gen_start_stop(sock, _target_ip, _target_port, _asic, _fpga, daq_push=_daq_push,gen_start_stop=_gen_start_stop, daq_start_stop=0x00, verbose=False):
    #                 logger.info(f"DAQ start/stop sent to {_target_ip}:{_target_port}")

    #         sock.close()
            
