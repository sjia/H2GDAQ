from PySide6.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QLabel, QPushButton, QSpinBox, QSizePolicy, QWidget, QCheckBox, QGridLayout, QMessageBox, QProgressBar, QApplication,QDialogButtonBox
from PySide6.QtCore import Qt, QCoreApplication
from PySide6.QtGui import QCursor, QPalette, QPixmap, QResizeEvent
import configparser
import sys, time, socket
import H2GDAQ_iodelay
import packetlib
from loguru import logger

log_format = "<level>{time:YYYY-MM-DD HH:mm:ss} | {level:.1s} | {message}</level>"
logger.remove()  # Remove default handler
logger.add(sink=sys.stderr, format=log_format)

class InitialSystemDialog(QDialog):
    def __init__(self, config_file, _version):
        super().__init__()
        self.config_file = config_file
        self.version = _version
        self.setGeometry(100, 100, 600, 400)
        self.initUI()
        self.loadConfig()
        
    def initUI(self):
        self.original_width = 600
        self.original_height = 400

        self.background_label = QLabel(self)
        self.background_label.setPixmap(QPixmap("asset/loading_page.png"))
        self.background_label.setScaledContents(True)
        self.background_label.setGeometry(0, 0, 600, 400)

        # FPGA number spinbox and label
        self.fpga_num_label = QLabel("Number of KCUs:", self)
        self.fpga_num_label.setStyleSheet("font-size: 14px; font-family: 'DejaVu Sans Mono';")
        self.fpga_num_label.setGeometry(380, 50, 200, 30)

        self.fpga_num_spinbox = QSpinBox(self)
        self.fpga_num_spinbox.setStyleSheet("font-size: 14px; font-family: 'DejaVu Sans Mono';")
        self.fpga_num_spinbox.setRange(1, 8)
        self.fpga_num_spinbox.setValue(1)
        self.fpga_num_spinbox.setFixedWidth(50)
        self.fpga_num_spinbox.setGeometry(380, 100, 200, 30)

        # ASIC number spinbox and label
        self.asic_num_label = QLabel("Number of ASICs per KCU:", self)
        self.asic_num_label.setStyleSheet("font-size: 14px; font-family: 'DejaVu Sans Mono';")
        self.asic_num_label.setGeometry(380, 200, 200, 30)

        self.asic_num_spinbox = QSpinBox(self)
        self.asic_num_spinbox.setStyleSheet("font-size: 14px; font-family: 'DejaVu Sans Mono';")
        self.asic_num_spinbox.setRange(1, 8)
        self.asic_num_spinbox.setValue(1)
        self.asic_num_spinbox.setFixedWidth(50)
        self.asic_num_spinbox.setGeometry(380, 250, 200, 30)

        # Confirm button
        self.confirm_button = QPushButton("Confirm", self)
        self.confirm_button.setStyleSheet("font-size: 14px; font-family: 'DejaVu Sans Mono';")
        self.confirm_button.setGeometry(380, 350, 200, 30)
        self.confirm_button.clicked.connect(self.onAccepted)

        # version info
        self.version_label = QLabel("v " + self.version, self)
        self.version_label.setStyleSheet("font-size: 14px; font-family: 'DejaVu Sans Mono';")
        self.version_label.setGeometry(30, 350, 200, 30)

        self.setWindowTitle("Initial System Dialog")

    def getSystemNumber(self):
        return {"fpga": self.fpga_num_spinbox.value(), "asic": self.asic_num_spinbox.value()}
    
    def resizeEvent(self, event: QResizeEvent):
        new_width = event.size().width()
        new_height = event.size().height()

        width_ratio = new_width / self.original_width
        height_ratio = new_height / self.original_height

        self.background_label.setGeometry(0, 0, new_width, new_height)

        self.fpga_num_label.setGeometry(int(380 * width_ratio), int(50 * height_ratio), int(200 * width_ratio), int(30 * height_ratio))
        self.fpga_num_spinbox.setGeometry(int(380 * width_ratio), int(100 * height_ratio), int(200 * width_ratio), int(30 * height_ratio))

        self.asic_num_label.setGeometry(int(380 * width_ratio), int(200 * height_ratio), int(200 * width_ratio), int(30 * height_ratio))
        self.asic_num_spinbox.setGeometry(int(380 * width_ratio), int(250 * height_ratio), int(200 * width_ratio), int(30 * height_ratio))

        self.confirm_button.setGeometry(int(380 * width_ratio), int(350 * height_ratio), int(200 * width_ratio), int(30 * height_ratio))

        self.version_label.setGeometry(int(30 * width_ratio), int(350 * height_ratio), int(200 * width_ratio), int(30 * height_ratio))

    
    def loadConfig(self):
        config = configparser.ConfigParser()
        config.read(self.config_file)
        num_fpgas = config.getint("Initial Settings", "num_fpgas", fallback=1)
        num_asics = config.getint("Initial Settings", "num_asics", fallback=1)
        self.fpga_num_spinbox.setValue(num_fpgas)
        self.asic_num_spinbox.setValue(num_asics)

    def onAccepted(self):
        config = configparser.ConfigParser()
        config.read(self.config_file)
        if not config.has_section("Initial Settings"):
            config.add_section("Initial Settings")
        config.set("Initial Settings", "num_fpgas", str(self.fpga_num_spinbox.value()))
        config.set("Initial Settings", "num_asics", str(self.asic_num_spinbox.value()))
        with open(self.config_file, "w") as configfile:
            config.write(configfile)
        self.accept()

class ASICSelectionDialog(QDialog):
    def __init__(self, mainwindow, position):
        super().__init__(mainwindow)  # Ensure QDialog is initialized with the parent widget
        self.mainwindow = mainwindow
        self.final_sel_fpga = -1
        self.final_sel_asic = -1
        self.asic_checkboxes = []
        self.initUI(position)  # Correctly call initUI with position only

    def initUI(self, position):
        self.setFixedSize(400, 150 + 20 * (self.mainwindow.num_asic_tabs // 4 + 1)*self.mainwindow.num_fpga_tabs)  # Set fixed size of dialog
        loc_x = position.x() - self.width() // 2  # Calculate new position for dialog
        loc_y = position.y() - self.height() // 2
        self.move(loc_x, loc_y)  # Move dialog to the new position

        self.setWindowTitle("ASIC Selection")

        dialog_layout = QVBoxLayout()

        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.asic_selection_widget = QWidget()
        self.asic_selection_layout = QVBoxLayout()

        for i in range(self.mainwindow.num_fpga_tabs):
            _fpga_label = QLabel("-- FPGA {} --".format(i), alignment=Qt.AlignCenter)
            _fpga_label.setStyleSheet("font-size: 14px; font-family: 'DejaVu Sans Mono';")
            self.asic_selection_layout.addWidget(_fpga_label)
            _asic_grid = QGridLayout()
            for j in range(self.mainwindow.num_asic_tabs):
                _asic_checkbox = QCheckBox("ASIC {}".format(j))
                _asic_checkbox.setStyleSheet("font-size: 12px; font-family: 'DejaVu Sans Mono';")
                _asic_checkbox.setChecked(False)
                _asic_checkbox.stateChanged.connect(lambda state, fpga=i, asic=j: self.onASICSelected(state, fpga, asic))
                _asic_grid.addWidget(_asic_checkbox, j // 4, j % 4)
                self.asic_checkboxes.append(_asic_checkbox)
            self.asic_selection_layout.addLayout(_asic_grid)
                
        self.asic_selection_widget.setLayout(self.asic_selection_layout)
        dialog_layout.addWidget(self.asic_selection_widget)
        dialog_layout.addStretch(1)
        dialog_layout.addWidget(self.buttonBox)

        self.setLayout(dialog_layout)

    def accept(self):
        logger.info(f"Final selection: FPGA {self.final_sel_fpga} ASIC {self.final_sel_asic}")
        if self.final_sel_fpga == -1 or self.final_sel_asic == -1:
            QMessageBox.warning(self, "Warning", "Please select an ASIC")
            return
        else:
            self.mainwindow.debug_sel_fpga = self.final_sel_fpga
            self.mainwindow.debug_sel_asic = self.final_sel_asic
            super().accept()

    def onASICSelected(self, state, fpga, asic):
        if state == 2:
            self.final_sel_fpga = fpga
            self.final_sel_asic = asic
            # disable all other checkboxes
            for i in range(self.mainwindow.num_fpga_tabs):
                for j in range(self.mainwindow.num_asic_tabs):
                    if i != fpga or j != asic:
                        self.asic_checkboxes[i * self.mainwindow.num_asic_tabs + j].setChecked(False)
        elif state == 0:
            # check if all checkboxes are unchecked
            if all([not _asic_checkbox.isChecked() for _asic_checkbox in self.asic_checkboxes]):
                self.final_sel_fpga = -1
                self.final_sel_asic = -1
        else:
            QMessageBox.warning(self, "Warning", "Invalid state")

class FPGASelectionDialog(QDialog):
    def __init__(self, mainwindow, position):
        super().__init__(mainwindow)  # Ensure QDialog is initialized with the parent widget
        self.mainwindow = mainwindow
        self.final_sel_fpga = -1
        self.fpga_checkboxes = []
        self.initUI(position)  # Correctly call initUI with position only

    def initUI(self, position):
        self.setFixedSize(400, 100 + 20 * self.mainwindow.num_fpga_tabs)  # Set fixed size of dialog
        loc_x = position.x() - self.width() // 2  # Calculate new position for dialog
        loc_y = position.y() - self.height() // 2
        self.move(loc_x, loc_y)  # Move dialog to the new position

        self.setWindowTitle("FPGA Selection")

        dialog_layout = QVBoxLayout()

        self.buttonBox = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)

        self.fpga_selection_widget = QWidget()
        self.fpga_selection_layout = QVBoxLayout()

        for i in range(self.mainwindow.num_fpga_tabs):
            _fpga_checkbox = QCheckBox("FPGA {}".format(i))
            _fpga_checkbox.setStyleSheet("font-size: 12px; font-family: 'DejaVu Sans Mono';")
            _fpga_checkbox.setChecked(False)
            _fpga_checkbox.stateChanged.connect(lambda state, fpga=i: self.onFPGASelected(state, fpga))
            self.fpga_selection_layout.addWidget(_fpga_checkbox)
            self.fpga_checkboxes.append(_fpga_checkbox)
                
        self.fpga_selection_widget.setLayout(self.fpga_selection_layout)
        dialog_layout.addWidget(self.fpga_selection_widget)
        dialog_layout.addStretch(1)
        dialog_layout.addWidget(self.buttonBox)

        self.setLayout(dialog_layout)

    def accept(self):
        logger.info(f"Final selection: FPGA {self.final_sel_fpga}")
        if self.final_sel_fpga == -1:
            QMessageBox.warning(self, "Warning", "Please select an ASIC")
            return
        else:
            self.mainwindow.reset_sel_fpga = self.final_sel_fpga
            super().accept()

    def onFPGASelected(self, state, fpga):
        if state == 2:
            self.final_sel_fpga = fpga
            # disable all other checkboxes
            for i in range(self.mainwindow.num_fpga_tabs):
                if i != fpga:
                    self.fpga_checkboxes[i].setChecked(False)
        elif state == 0:
            # check if all checkboxes are unchecked
            if all([not _fpga_checkbox.isChecked() for _fpga_checkbox in self.fpga_checkboxes]):
                self.final_sel_fpga = -1
        else:
            QMessageBox.warning(self, "Warning", "Invalid state")


class IODelaySettingDialog(QDialog):
    def __init__(self, mainwindow, num_fpga, position):
        super().__init__(mainwindow)
        self.fpga_sel = [False] * num_fpga
        self.mainwindow = mainwindow
        self.fpga_checkboxes = []
        self.initUI(num_fpga, position)
    
    def initUI(self, num_fpga, position):
        _iodelay_layout = QVBoxLayout()
        _all_option_checkbox = QCheckBox("All FPGA")
        _all_option_checkbox.setStyleSheet("font-size: 12px; font-family: 'DejaVu Sans Mono';")
        _all_option_checkbox.setChecked(False)
        _all_option_checkbox.stateChanged.connect(lambda state: self.on_all_option_checked(state))
        _iodelay_layout.addWidget(_all_option_checkbox)

        _fpga_layout = QGridLayout()

        for i in range(num_fpga):
            _fpga_checkbox = QCheckBox("FPGA {}".format(i))
            _fpga_checkbox.setStyleSheet("font-size: 12px; font-family: 'DejaVu Sans Mono';")
            _fpga_checkbox.setChecked(False)
            _fpga_layout.addWidget(_fpga_checkbox, i//2, i%2)
            _fpga_checkbox.stateChanged.connect(lambda state, fpga=i: self.on_fpga_checked(state, fpga))
            self.fpga_checkboxes.append(_fpga_checkbox)

        _iodelay_layout.addLayout(_fpga_layout)

        self.setLayout(_iodelay_layout)

        self.accept_button = QPushButton("Accept")
        self.accept_button.setStyleSheet("font-size: 12px; font-family: 'DejaVu Sans Mono';")
        self.accept_button.setFixedHeight(30)
        self.accept_button.clicked.connect(self.accept)

        self.cancel_button = QPushButton("Cancel")
        self.cancel_button.setStyleSheet("font-size: 12px; font-family: 'DejaVu Sans Mono';")
        self.cancel_button.setFixedHeight(30)
        self.cancel_button.clicked.connect(self.reject)

        _button_layout = QHBoxLayout()
        _button_layout.addWidget(self.accept_button)
        _button_layout.addWidget(self.cancel_button)

        _iodelay_layout.addLayout(_button_layout)

        self.setWindowTitle("IO Delay Setting")

        self.setFixedSize(300, 70 + 30 * (num_fpga // 2 + 1))
        # set the position of the dialog to the center of the screen
        self.move(position.x() - self.width() / 2, position.y() - self.height() / 2)

    def accept(self):
        if not any(self.fpga_sel):
            QMessageBox.warning(self, "Warning", "Please select at least one FPGA")
            return
        # * Start the IO delay setting
        asic_number = 2

        range_corse = range(0, 512, 2)
        step_fine   = 1

        locked_output       = 0xaccccccc
        sublist_min_len     = 20
        sublist_extend      = 8
        inter_step_sleep    = 0.05 # seconds

        asic_select         = 0x03

        enable_sublist          = True
        enable_reset            = True

        i2c_setting_verbose     = False
        bitslip_verbose         = False
        bitslip_debug_verbose   = False

        _socket_udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        _socket_udp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            _socket_udp.bind((self.mainwindow.pc_ip, self.mainwindow.pc_port))
        except Exception as e:
            QMessageBox.warning(self, "Error", "Error in binding socket: {}".format(e))
            _socket_udp.close()
            self.close()
            return
        _socket_udp.settimeout(3)

        _progress_dialog = QDialog()
        _progress_dialog_layout = QVBoxLayout()

        _progress_bar_fpga = QProgressBar()
        _progress_bar_fpga.setRange(0, 100)
        _progress_bar_fpga.setValue(0)
        _progress_dialog_layout.addWidget(_progress_bar_fpga)

        _progress_bar_step = QProgressBar()
        _progress_bar_step.setRange(0, 100)
        _progress_bar_step.setValue(0)
        # set text prefix
        _progress_dialog_layout.addWidget(_progress_bar_step)

        _info_label = QLabel("Setting IO Delay")
        _info_label.setStyleSheet("font-size: 12px; font-family: 'DejaVu Sans Mono';")
        _progress_dialog_layout.addWidget(_info_label)

        _progress_dialog.setLayout(_progress_dialog_layout)
        _progress_dialog.setWindowTitle("Setting IO Delay")
        _progress_dialog.setFixedSize(500, 100)
        _progress_dialog.move(self.pos().x() + self.width() / 2 - _progress_dialog.width() / 2, self.pos().y() + self.height() / 2 - _progress_dialog.height() / 2)

        self.hide()
        _progress_dialog.show()

        best_values = []
        asic_total_cnt = 0

        _progress_bar_fpga.setFormat("0 / {}".format(sum(self.fpga_sel) * asic_number))
        total_fpga = sum(self.fpga_sel)
        try:
            for fpga_index, fpga_enabled in enumerate(self.fpga_sel):
                if fpga_enabled:
                    # * Set first scan
                    best_values.append([])
                    for asic_index in range(asic_number):
                        _target_ip = self.mainwindow.link_ip_address_list[fpga_index]
                        _target_port = int(self.mainwindow.link_port_list[fpga_index])
                        _label_prefix = "[FPGA {} ASIC {}]".format(fpga_index, asic_index)
                        QApplication.processEvents()
                        H2GDAQ_iodelay.send_iodelay_i2c(_socket_udp, _target_ip, _target_port, fpga_index, asic_index)
                        time.sleep(0.1)
                        if not packetlib.send_reset_adj(_socket_udp,  _target_ip, _target_port, fpga_addr=fpga_index, asic_num=asic_index, sw_hard_reset_sel=0x00, sw_hard_reset=0x00, sw_soft_reset_sel=(1<<asic_index), sw_soft_reset=0x01, sw_i2c_reset_sel=0x00, sw_i2c_reset=0x00, reset_pack_counter=0x00, adjustable_start=0x00, verbose=False):
                            logger.critical("Error in resetting ASIC")
                            QMessageBox.critical(self, "Error", "Error in resetting ASIC")
                            return
                        locked_flag_array  = []
                        locked_delay_array = []
                        logger.info(f"Setting IO Delay: {fpga_index} Scan {asic_index}")
                        for _delay in range_corse:
                            _info_label.setText(_label_prefix + " delay " + str(_delay))
                            locked_flag_array.append(H2GDAQ_iodelay.test_delay(_socket_udp, _target_ip, _target_port, fpga_index, _delay, asic_index, verbose=False))
                            locked_delay_array.append(_delay)
                            _progress_bar_step.setValue(int(float(_delay) / 512.0 * 50.0))
                        valid_sublists = []
                        try:
                            valid_sublists = H2GDAQ_iodelay.find_true_sublists(locked_flag_array)
                        except Exception as e:
                            logger.error(f"Error in finding true sublists: {e}")
                            continue
                        if len(valid_sublists) == 0:
                            logger.error("No valid sublists found")
                            QMessageBox.critical(self, "Error", "No valid delay value found for FPGA {} ASIC {}".format(fpga_index, asic_index))
                            # throw error
                            return

                        sorted_sublists = sorted(valid_sublists, key=lambda x: x[1], reverse=True)
                        _valid_sublist_found = False
                        _best_delay = 0

                        for _sublist_index in range(len(sorted_sublists)):
                            _sublist = sorted_sublists[_sublist_index]

                            if len(_sublist) != 2:
                                logger.warning(f"Abnormal sublist data format for ASIC {asic_index}")
                                break
                            _start_index = _sublist[0]
                            _sublist_len = _sublist[1]
                            if _sublist_len < sublist_min_len:
                                _best_delay = _start_index + _sublist_len // 2
                                _valid_sublist_found = True
                                logger.warning('No best IO delay found for ASIC ' + str(asic_index)+ ' using coarse delay ' + str(_best_delay))
                                break
                            if not enable_sublist:
                                _best_delay = _start_index + _sublist_len // 2
                                _valid_sublist_found = True
                                break
                            else:
                                _subscan_start = max(0, _start_index - sublist_extend)
                                _subscan_end = min(511, _start_index + _sublist_len + sublist_extend)

                            valid_subsublists = []
                            locked_flag_sublist = []
                            locked_delay_sublist = []
                            progress_bar_sublocal = range(_subscan_start, _subscan_end, 1)
                            for _subdelay in progress_bar_sublocal:
                                locked_flag_sublist.append(H2GDAQ_iodelay.test_delay(_socket_udp, _target_ip, _target_port, fpga_index, _subdelay, asic_index, verbose=False))
                                locked_delay_sublist.append(_subdelay)
                                _progress_bar_step.setValue(int(50.0 + float(_subdelay - _subscan_start) / float(_subscan_end - _subscan_start) * 50.0))
                                _info_label.setText(_label_prefix + " delay " + str(_subdelay))
                            try:
                                valid_subsublists = H2GDAQ_iodelay.find_true_sublists(locked_flag_sublist)
                            except:
                                continue
                            if len(valid_subsublists) == 0:
                                continue
                            sorted_subsublists = sorted(valid_subsublists, key=lambda x: x[1], reverse=True)
                            try:
                                best_sublist = sorted_subsublists[0]
                            except:
                                continue
                            if best_sublist[1] > sublist_min_len:
                                _best_delay = best_sublist[0] + best_sublist[1] // 2 + _subscan_start
                                _valid_sublist_found = True
                                break
                            else:
                                if _sublist_index == len(sorted_sublists) - 1:
                                    _best_delay = _start_index + _sublist_len // 2
                                    _valid_sublist_found = True
                                    logger.warning('No best IO delay found for ASIC ' + str(asic_index)+ ' using coarse delay ' + str(_best_delay) + ' (E4)')
                                    break
                                else:
                                    continue
                        if not _valid_sublist_found:
                            logger.error('No valid IO delay found for ASIC ' + str(asic_index))
                            continue
                        if not H2GDAQ_iodelay.test_delay(_socket_udp, _target_ip, _target_port, fpga_index, _best_delay, asic_index, verbose=False):
                            logger.error(f"Best IO delay candidate for ASIC {asic_index} is not locked")
                            continue
                        else:
                            logger.info(f"Best IO delay for ASIC {asic_index}: {_best_delay}")
                            best_values[-1].append(_best_delay)
                        _info_label.setText(_label_prefix + " Setting IO Delay: Scan 1")
                        asic_total_cnt += 1
                        _progress_bar_fpga.setValue(int(asic_total_cnt / (total_fpga * asic_number) * 100))
                        _progress_bar_fpga.setFormat(f"{asic_total_cnt} / {total_fpga * asic_number}")
                        QApplication.processEvents()
        finally:
            _socket_udp.close()
            _progress_dialog.close()
        _progress_dialog.close()
        _result_str = "Best IO Delay Values:\n"
        for i in range(len(best_values)):
            for j in range(len(best_values[i])):
                _result_str += "FPGA {} ASIC {}: {}\n".format(i, j, best_values[i][j])
        QMessageBox.information(self, "IO Delay Setting Succeeded", _result_str)
        self.close()

    def on_all_option_checked(self, state):
        if state == 2:
            for _button in self.fpga_checkboxes:
                _button.setChecked(True)
        elif state == 0:
            for _button in self.fpga_checkboxes:
                _button.setChecked(False)
        else:
            QMessageBox.warning(self, "Warning", "Invalid state")

    def on_fpga_checked(self, state, fpga):
        if state == 2:
            self.fpga_sel[fpga] = True
        elif state == 0:
            self.fpga_sel[fpga] = False
        else:
            QMessageBox.warning(self, "Warning", "Invalid state")






