import packetlib
import socket
import numpy as np
import time
import json
import os, sys
from loguru import logger

log_format = "<level>{time:YYYY-MM-DD HH:mm:ss} | {level:.1s} | {message}</level>"
logger.remove()  # Remove default handler
logger.add(sink=sys.stderr, format=log_format)

locked_output       = 0xaccccccc
sublist_min_len     = 20
sublist_extend      = 8
inter_step_sleep    = 0.05 # seconds

asic_select         = 0x03

enable_sublist          = True
enable_reset            = True

i2c_setting_verbose     = False
bitslip_verbose         = False
bitslip_debug_verbose   = False

def find_true_sublists(bool_list):
    results = []
    start_index = None
    in_sequence = False

    for index, value in enumerate(bool_list):
        if value:
            if not in_sequence:
                # Starting a new sequence
                start_index = index
                in_sequence = True
        else:
            if in_sequence:
                # Ending a sequence
                results.append((start_index, index - start_index))
                in_sequence = False

    # Check if the last sequence extends to the end of the list
    if in_sequence:
        results.append((start_index, len(bool_list) - start_index))

    return results

def test_delay(socket_udp, h2gcroc_ip, h2gcroc_port, fpga_address, delay_val, num_asic, verbose=False):
    if not packetlib.set_bitslip(socket_udp, h2gcroc_ip, h2gcroc_port, fpga_addr=fpga_address, asic_num=num_asic, io_dly_sel=asic_select, a0_io_dly_val_fclk=0x000, a0_io_dly_val_fcmd=0x400, a1_io_dly_val_fclk=0x000, a1_io_dly_val_fcmd=0x400, a0_io_dly_val_tr0=delay_val, a0_io_dly_val_tr1=delay_val, a0_io_dly_val_tr2=delay_val, a0_io_dly_val_tr3=delay_val, a0_io_dly_val_dq0=delay_val, a0_io_dly_val_dq1=delay_val, a1_io_dly_val_tr0=delay_val, a1_io_dly_val_tr1=delay_val, a1_io_dly_val_tr2=delay_val, a1_io_dly_val_tr3=delay_val, a1_io_dly_val_dq0=delay_val, a1_io_dly_val_dq1=delay_val, verbose=bitslip_verbose):
        print('\033[33m' + "Warning in setting bitslip 0" + '\033[0m')
    if not packetlib.send_reset_adj(socket_udp, h2gcroc_ip, h2gcroc_port,fpga_addr=fpga_address, asic_num=num_asic, sw_hard_reset_sel=0x00, sw_hard_reset=0x00,sw_soft_reset_sel=0x00, sw_soft_reset=0x00, sw_i2c_reset_sel=0x00,sw_i2c_reset=0x00, reset_pack_counter=0x00, adjustable_start=asic_select,verbose=False):
        print('\033[33m' + "Warning in sending reset_adj" + '\033[0m')
    time.sleep(inter_step_sleep)
    debug_info = packetlib.get_debug_data(socket_udp, h2gcroc_ip, h2gcroc_port,fpga_addr=fpga_address, asic_num=num_asic, verbose=bitslip_debug_verbose)
    if debug_info is None:
        print('\033[33m' + "Warning in getting debug data" + '\033[0m')
    else:
        print_str = "Delay " + "{:03}".format(delay_val) + ": "
        all_locked = True
        if debug_info["trg0_value"] == locked_output:
            if verbose:
                print_str += '\033[32m' + "T0 " + '\033[0m'
        else:
            if verbose:
                print_str += '\033[31m' + "T0 " + '\033[0m'
            all_locked = False
        if debug_info["trg1_value"] == locked_output:
            if verbose:
                print_str += '\033[32m' + "T1 " + '\033[0m'
        else:
            if verbose:
                print_str += '\033[31m' + "T1 " + '\033[0m'
            all_locked = False
        if debug_info["trg2_value"] == locked_output:
            if verbose:
                print_str += '\033[32m' + "T2 " + '\033[0m'
        else:
            if verbose:
                print_str += '\033[31m' + "T2 " + '\033[0m'
            all_locked = False
        if debug_info["trg3_value"] == locked_output:
            if verbose:
                print_str += '\033[32m' + "T3 " + '\033[0m'
        else:
            if verbose:
                print_str += '\033[31m' + "T3 " + '\033[0m'
            all_locked = False
        if debug_info["data0_value"] == locked_output:
            if verbose:
                print_str += '\033[32m' + "D0 " + '\033[0m'
        else:
            if verbose:
                print_str += '\033[31m' + "D0 " + '\033[0m'
            all_locked = False
        if debug_info["data1_value"] == locked_output:
            if verbose:
                print_str += '\033[32m' + "D1 " + '\033[0m'
        else:
            if verbose:
                print_str += '\033[31m' + "D1 " + '\033[0m'
            all_locked = False
        if verbose:
            print(print_str)
    return all_locked

i2c_content_top = [0x08,0x0f,0x40,0x7f,0x00,0x07,0x05,0x00]
i2c_content_digital_half_0 = [0x00,0x00,0x00,0x00,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x19,0x00,0x0a,0xcc,0xcc,0xcc,0x0c,0xcc,0xcc,0xcc,0xcc,0x0f,0x02,0x00]
i2c_content_digital_half_1 = [0x00,0x00,0x00,0x00,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x19,0x00,0x0a,0xcc,0xcc,0xcc,0x0c,0xcc,0xcc,0xcc,0xcc,0x0f,0x02,0x00]
i2c_content_global_analog_0 =[0x6f,0xdb,0x83,0x28,0x28,0x28,0x9a,0x9a,0xa8,0x8a,0x40,0x4a,0x4b,0x68]
i2c_content_global_analog_1 =[0x6f,0xdb,0x83,0x28,0x28,0x28,0x9a,0x9a,0xa8,0x8a,0x40,0x4a,0x4b,0x68]
i2c_content_master_tdc_0 = [0x37,0xd4,0x54,0x80,0x0a,0xd4,0x03,0x00,0x80,0x80,0x0a,0x95,0x03,0x00,0x40,0x00]
i2c_content_master_tdc_1 = [0x37,0xd4,0x54,0x80,0x0a,0xd4,0x03,0x00,0x80,0x80,0x0a,0x95,0x03,0x00,0x40,0x00]
i2c_content_reference_voltage_0 = [0xb4,0x0a,0xfa,0xfa,0xb8,0xd4,0xda,0x42,0x00,0x00]
i2c_content_reference_voltage_1 = [0xb4,0x0e,0xfa,0xfa,0xad,0xd4,0xda,0x42,0x00,0x00]
i2c_content_half_wise_0 = [0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00]
i2c_content_half_wise_1 = [0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00]

def send_iodelay_i2c(socket_udp, h2gcroc_ip, h2gcroc_port, fpga_address, _asic, i2c_setting_verbose=False):
    if True:
        # logger.info(f"Setting I2C for ASIC {_asic} ...")
        if not packetlib.send_check_i2c_wrapper(socket_udp, h2gcroc_ip, h2gcroc_port, asic_num=_asic, fpga_addr = fpga_address, sub_addr=packetlib.subblock_address_dict["Top"], reg_addr=0x00, data=i2c_content_top,retry=3, verbose=i2c_setting_verbose):
            logger.warning(f"Readback mismatch for ASIC {_asic} Top")
        # if not packetlib.send_check_i2c_wrapper(socket_udp, h2gcroc_ip, h2gcroc_port, asic_num=_asic, fpga_addr = fpga_address, sub_addr=packetlib.subblock_address_dict["Digital_Half_0"], reg_addr=0x00, data=i2c_content_digital_half_0,retry=3, verbose=i2c_setting_verbose):
        #     logger.warning(f"Readback mismatch for ASIC {_asic} Digital_Half_0")
        # if not packetlib.send_check_i2c_wrapper(socket_udp, h2gcroc_ip, h2gcroc_port, asic_num=_asic, fpga_addr = fpga_address, sub_addr=packetlib.subblock_address_dict["Digital_Half_1"], reg_addr=0x00, data=i2c_content_digital_half_1,retry=3, verbose=i2c_setting_verbose):
        #     logger.warning(f"Readback mismatch for ASIC {_asic} Digital_Half_1")
        # if not packetlib.send_check_i2c_wrapper(socket_udp, h2gcroc_ip, h2gcroc_port, asic_num=_asic, fpga_addr = fpga_address, sub_addr=packetlib.subblock_address_dict["Global_Analog_0"], reg_addr=0x00, data=i2c_content_global_analog_0,retry=3, verbose=i2c_setting_verbose):
        #     logger.warning(f"Readback mismatch for ASIC {_asic} Global_Analog_0")
        # if not packetlib.send_check_i2c_wrapper(socket_udp, h2gcroc_ip, h2gcroc_port, asic_num=_asic, fpga_addr = fpga_address, sub_addr=packetlib.subblock_address_dict["Global_Analog_1"], reg_addr=0x00, data=i2c_content_global_analog_1,retry=3, verbose=i2c_setting_verbose):
        #     logger.warning(f"Readback mismatch for ASIC {_asic} Global_Analog_1")
        # if not packetlib.send_check_i2c_wrapper(socket_udp, h2gcroc_ip, h2gcroc_port, asic_num=_asic, fpga_addr = fpga_address, sub_addr=packetlib.subblock_address_dict["Master_TDC_0"], reg_addr=0x00, data=i2c_content_master_tdc_0,retry=3, verbose=i2c_setting_verbose):
        #     logger.warning(f"Readback mismatch for ASIC {_asic} Master_TDC_0")
        # if not packetlib.send_check_i2c_wrapper(socket_udp, h2gcroc_ip, h2gcroc_port, asic_num=_asic, fpga_addr = fpga_address, sub_addr=packetlib.subblock_address_dict["Master_TDC_1"], reg_addr=0x00, data=i2c_content_master_tdc_1,retry=3, verbose=i2c_setting_verbose):
        #     logger.warning(f"Readback mismatch for ASIC {_asic} Master_TDC_1")
        # if not packetlib.send_check_i2c_wrapper(socket_udp, h2gcroc_ip, h2gcroc_port, asic_num=_asic, fpga_addr = fpga_address, sub_addr=packetlib.subblock_address_dict["Reference_Voltage_0"], reg_addr=0x00, data=i2c_content_reference_voltage_0,retry=3, verbose=i2c_setting_verbose):
        #     logger.warning(f"Readback mismatch for ASIC {_asic} Reference_Voltage_0")
        # if not packetlib.send_check_i2c_wrapper(socket_udp, h2gcroc_ip, h2gcroc_port, asic_num=_asic, fpga_addr = fpga_address, sub_addr=packetlib.subblock_address_dict["Reference_Voltage_1"], reg_addr=0x00, data=i2c_content_reference_voltage_1,retry=3, verbose=i2c_setting_verbose):
        #     logger.warning(f"Readback mismatch for ASIC {_asic} Reference_Voltage_1")
        # # ! HalfWise will not read back correctly
        # packetlib.send_check_i2c_wrapper(socket_udp, h2gcroc_ip, h2gcroc_port, asic_num=_asic, fpga_addr = fpga_address, sub_addr=packetlib.subblock_address_dict["HalfWise_0"], reg_addr=0x00, data=i2c_content_half_wise_0,retry=3, verbose=i2c_setting_verbose)
        # packetlib.send_check_i2c_wrapper(socket_udp, h2gcroc_ip, h2gcroc_port, asic_num=_asic, fpga_addr = fpga_address, sub_addr=packetlib.subblock_address_dict["HalfWise_1"], reg_addr=0x00, data=i2c_content_half_wise_1,retry=3, verbose=i2c_setting_verbose)

